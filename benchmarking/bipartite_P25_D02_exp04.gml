graph [
  directed 1
  name "complete_bipartite_graph(25,3)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 16
    label "16"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 17
    label "17"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 18
    label "18"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 19
    label "19"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 20
    label "20"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 21
    label "21"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 22
    label "22"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 23
    label "23"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 24
    label "24"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 25
    label "25"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 26
    label "26"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 27
    label "27"
    bipartite 1
    availableSeats 17
    waitingList 1
  ]
  edge [
    source 0
    target 25
    preference 43
  ]
  edge [
    source 0
    target 26
    preference 53
  ]
  edge [
    source 0
    target 27
    preference 1
  ]
  edge [
    source 1
    target 25
    preference 19
  ]
  edge [
    source 1
    target 26
    preference 21
  ]
  edge [
    source 1
    target 27
    preference 1
  ]
  edge [
    source 2
    target 25
    preference 40
  ]
  edge [
    source 2
    target 26
    preference 3
  ]
  edge [
    source 2
    target 27
    preference 1
  ]
  edge [
    source 3
    target 25
    preference 19
  ]
  edge [
    source 3
    target 26
    preference 55
  ]
  edge [
    source 3
    target 27
    preference 1
  ]
  edge [
    source 4
    target 25
    preference 8
  ]
  edge [
    source 4
    target 26
    preference 42
  ]
  edge [
    source 4
    target 27
    preference 1
  ]
  edge [
    source 5
    target 25
    preference 30
  ]
  edge [
    source 5
    target 26
    preference 40
  ]
  edge [
    source 5
    target 27
    preference 1
  ]
  edge [
    source 6
    target 25
    preference 4
  ]
  edge [
    source 6
    target 26
    preference 7
  ]
  edge [
    source 6
    target 27
    preference 1
  ]
  edge [
    source 7
    target 25
    preference 39
  ]
  edge [
    source 7
    target 26
    preference 29
  ]
  edge [
    source 7
    target 27
    preference 1
  ]
  edge [
    source 8
    target 25
    preference 56
  ]
  edge [
    source 8
    target 26
    preference 41
  ]
  edge [
    source 8
    target 27
    preference 1
  ]
  edge [
    source 9
    target 25
    preference 55
  ]
  edge [
    source 9
    target 26
    preference 43
  ]
  edge [
    source 9
    target 27
    preference 1
  ]
  edge [
    source 10
    target 25
    preference 53
  ]
  edge [
    source 10
    target 26
    preference 49
  ]
  edge [
    source 10
    target 27
    preference 1
  ]
  edge [
    source 11
    target 25
    preference 26
  ]
  edge [
    source 11
    target 26
    preference 56
  ]
  edge [
    source 11
    target 27
    preference 1
  ]
  edge [
    source 12
    target 25
    preference 55
  ]
  edge [
    source 12
    target 26
    preference 8
  ]
  edge [
    source 12
    target 27
    preference 1
  ]
  edge [
    source 13
    target 25
    preference 19
  ]
  edge [
    source 13
    target 26
    preference 31
  ]
  edge [
    source 13
    target 27
    preference 1
  ]
  edge [
    source 14
    target 25
    preference 35
  ]
  edge [
    source 14
    target 26
    preference 12
  ]
  edge [
    source 14
    target 27
    preference 1
  ]
  edge [
    source 15
    target 25
    preference 42
  ]
  edge [
    source 15
    target 26
    preference 43
  ]
  edge [
    source 15
    target 27
    preference 1
  ]
  edge [
    source 16
    target 25
    preference 26
  ]
  edge [
    source 16
    target 26
    preference 10
  ]
  edge [
    source 16
    target 27
    preference 1
  ]
  edge [
    source 17
    target 25
    preference 56
  ]
  edge [
    source 17
    target 26
    preference 23
  ]
  edge [
    source 17
    target 27
    preference 1
  ]
  edge [
    source 18
    target 25
    preference 20
  ]
  edge [
    source 18
    target 26
    preference 12
  ]
  edge [
    source 18
    target 27
    preference 1
  ]
  edge [
    source 19
    target 25
    preference 38
  ]
  edge [
    source 19
    target 26
    preference 40
  ]
  edge [
    source 19
    target 27
    preference 1
  ]
  edge [
    source 20
    target 25
    preference 25
  ]
  edge [
    source 20
    target 26
    preference 22
  ]
  edge [
    source 20
    target 27
    preference 1
  ]
  edge [
    source 21
    target 25
    preference 9
  ]
  edge [
    source 21
    target 26
    preference 2
  ]
  edge [
    source 21
    target 27
    preference 1
  ]
  edge [
    source 22
    target 25
    preference 6
  ]
  edge [
    source 22
    target 26
    preference 7
  ]
  edge [
    source 22
    target 27
    preference 1
  ]
  edge [
    source 23
    target 25
    preference 20
  ]
  edge [
    source 23
    target 26
    preference 15
  ]
  edge [
    source 23
    target 27
    preference 1
  ]
  edge [
    source 24
    target 25
    preference 11
  ]
  edge [
    source 24
    target 26
    preference 56
  ]
  edge [
    source 24
    target 27
    preference 1
  ]
  edge [
    source 25
    target 0
    preference 10
  ]
  edge [
    source 25
    target 1
    preference 36
  ]
  edge [
    source 25
    target 2
    preference 33
  ]
  edge [
    source 25
    target 3
    preference 25
  ]
  edge [
    source 25
    target 4
    preference 54
  ]
  edge [
    source 25
    target 5
    preference 41
  ]
  edge [
    source 25
    target 6
    preference 6
  ]
  edge [
    source 25
    target 7
    preference 19
  ]
  edge [
    source 25
    target 8
    preference 19
  ]
  edge [
    source 25
    target 9
    preference 42
  ]
  edge [
    source 25
    target 10
    preference 41
  ]
  edge [
    source 25
    target 11
    preference 39
  ]
  edge [
    source 25
    target 12
    preference 43
  ]
  edge [
    source 25
    target 13
    preference 27
  ]
  edge [
    source 25
    target 14
    preference 28
  ]
  edge [
    source 25
    target 15
    preference 45
  ]
  edge [
    source 25
    target 16
    preference 35
  ]
  edge [
    source 25
    target 17
    preference 24
  ]
  edge [
    source 25
    target 18
    preference 53
  ]
  edge [
    source 25
    target 19
    preference 51
  ]
  edge [
    source 25
    target 20
    preference 25
  ]
  edge [
    source 25
    target 21
    preference 8
  ]
  edge [
    source 25
    target 22
    preference 35
  ]
  edge [
    source 25
    target 23
    preference 22
  ]
  edge [
    source 25
    target 24
    preference 12
  ]
  edge [
    source 26
    target 0
    preference 37
  ]
  edge [
    source 26
    target 1
    preference 19
  ]
  edge [
    source 26
    target 2
    preference 6
  ]
  edge [
    source 26
    target 3
    preference 5
  ]
  edge [
    source 26
    target 4
    preference 55
  ]
  edge [
    source 26
    target 5
    preference 37
  ]
  edge [
    source 26
    target 6
    preference 26
  ]
  edge [
    source 26
    target 7
    preference 42
  ]
  edge [
    source 26
    target 8
    preference 10
  ]
  edge [
    source 26
    target 9
    preference 36
  ]
  edge [
    source 26
    target 10
    preference 10
  ]
  edge [
    source 26
    target 11
    preference 38
  ]
  edge [
    source 26
    target 12
    preference 32
  ]
  edge [
    source 26
    target 13
    preference 31
  ]
  edge [
    source 26
    target 14
    preference 5
  ]
  edge [
    source 26
    target 15
    preference 18
  ]
  edge [
    source 26
    target 16
    preference 19
  ]
  edge [
    source 26
    target 17
    preference 42
  ]
  edge [
    source 26
    target 18
    preference 29
  ]
  edge [
    source 26
    target 19
    preference 7
  ]
  edge [
    source 26
    target 20
    preference 55
  ]
  edge [
    source 26
    target 21
    preference 7
  ]
  edge [
    source 26
    target 22
    preference 25
  ]
  edge [
    source 26
    target 23
    preference 45
  ]
  edge [
    source 26
    target 24
    preference 56
  ]
  edge [
    source 27
    target 0
    preference 1
  ]
  edge [
    source 27
    target 1
    preference 1
  ]
  edge [
    source 27
    target 2
    preference 1
  ]
  edge [
    source 27
    target 3
    preference 1
  ]
  edge [
    source 27
    target 4
    preference 1
  ]
  edge [
    source 27
    target 5
    preference 1
  ]
  edge [
    source 27
    target 6
    preference 1
  ]
  edge [
    source 27
    target 7
    preference 1
  ]
  edge [
    source 27
    target 8
    preference 1
  ]
  edge [
    source 27
    target 9
    preference 1
  ]
  edge [
    source 27
    target 10
    preference 1
  ]
  edge [
    source 27
    target 11
    preference 1
  ]
  edge [
    source 27
    target 12
    preference 1
  ]
  edge [
    source 27
    target 13
    preference 1
  ]
  edge [
    source 27
    target 14
    preference 1
  ]
  edge [
    source 27
    target 15
    preference 1
  ]
  edge [
    source 27
    target 16
    preference 1
  ]
  edge [
    source 27
    target 17
    preference 1
  ]
  edge [
    source 27
    target 18
    preference 1
  ]
  edge [
    source 27
    target 19
    preference 1
  ]
  edge [
    source 27
    target 20
    preference 1
  ]
  edge [
    source 27
    target 21
    preference 1
  ]
  edge [
    source 27
    target 22
    preference 1
  ]
  edge [
    source 27
    target 23
    preference 1
  ]
  edge [
    source 27
    target 24
    preference 1
  ]
]
