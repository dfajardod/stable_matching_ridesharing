graph [
  directed 1
  name "complete_bipartite_graph(30,7)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 16
    label "16"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 17
    label "17"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 18
    label "18"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 19
    label "19"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 20
    label "20"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 21
    label "21"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 22
    label "22"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 23
    label "23"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 24
    label "24"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 25
    label "25"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 26
    label "26"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 27
    label "27"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 28
    label "28"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 29
    label "29"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 30
    label "30"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 31
    label "31"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 32
    label "32"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 33
    label "33"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 34
    label "34"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 35
    label "35"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 36
    label "36"
    bipartite 1
    availableSeats 6
    waitingList 1
  ]
  edge [
    source 0
    target 30
    preference 54
  ]
  edge [
    source 0
    target 31
    preference 54
  ]
  edge [
    source 0
    target 32
    preference 38
  ]
  edge [
    source 0
    target 33
    preference 61
  ]
  edge [
    source 0
    target 34
    preference 70
  ]
  edge [
    source 0
    target 35
    preference 28
  ]
  edge [
    source 0
    target 36
    preference 1
  ]
  edge [
    source 1
    target 30
    preference 28
  ]
  edge [
    source 1
    target 31
    preference 68
  ]
  edge [
    source 1
    target 32
    preference 12
  ]
  edge [
    source 1
    target 33
    preference 18
  ]
  edge [
    source 1
    target 34
    preference 37
  ]
  edge [
    source 1
    target 35
    preference 71
  ]
  edge [
    source 1
    target 36
    preference 1
  ]
  edge [
    source 2
    target 30
    preference 59
  ]
  edge [
    source 2
    target 31
    preference 44
  ]
  edge [
    source 2
    target 32
    preference 31
  ]
  edge [
    source 2
    target 33
    preference 2
  ]
  edge [
    source 2
    target 34
    preference 26
  ]
  edge [
    source 2
    target 35
    preference 72
  ]
  edge [
    source 2
    target 36
    preference 1
  ]
  edge [
    source 3
    target 30
    preference 50
  ]
  edge [
    source 3
    target 31
    preference 25
  ]
  edge [
    source 3
    target 32
    preference 24
  ]
  edge [
    source 3
    target 33
    preference 20
  ]
  edge [
    source 3
    target 34
    preference 69
  ]
  edge [
    source 3
    target 35
    preference 17
  ]
  edge [
    source 3
    target 36
    preference 1
  ]
  edge [
    source 4
    target 30
    preference 3
  ]
  edge [
    source 4
    target 31
    preference 20
  ]
  edge [
    source 4
    target 32
    preference 6
  ]
  edge [
    source 4
    target 33
    preference 58
  ]
  edge [
    source 4
    target 34
    preference 69
  ]
  edge [
    source 4
    target 35
    preference 49
  ]
  edge [
    source 4
    target 36
    preference 1
  ]
  edge [
    source 5
    target 30
    preference 47
  ]
  edge [
    source 5
    target 31
    preference 8
  ]
  edge [
    source 5
    target 32
    preference 64
  ]
  edge [
    source 5
    target 33
    preference 35
  ]
  edge [
    source 5
    target 34
    preference 15
  ]
  edge [
    source 5
    target 35
    preference 2
  ]
  edge [
    source 5
    target 36
    preference 1
  ]
  edge [
    source 6
    target 30
    preference 38
  ]
  edge [
    source 6
    target 31
    preference 31
  ]
  edge [
    source 6
    target 32
    preference 49
  ]
  edge [
    source 6
    target 33
    preference 55
  ]
  edge [
    source 6
    target 34
    preference 15
  ]
  edge [
    source 6
    target 35
    preference 74
  ]
  edge [
    source 6
    target 36
    preference 1
  ]
  edge [
    source 7
    target 30
    preference 40
  ]
  edge [
    source 7
    target 31
    preference 57
  ]
  edge [
    source 7
    target 32
    preference 31
  ]
  edge [
    source 7
    target 33
    preference 55
  ]
  edge [
    source 7
    target 34
    preference 18
  ]
  edge [
    source 7
    target 35
    preference 2
  ]
  edge [
    source 7
    target 36
    preference 1
  ]
  edge [
    source 8
    target 30
    preference 62
  ]
  edge [
    source 8
    target 31
    preference 57
  ]
  edge [
    source 8
    target 32
    preference 27
  ]
  edge [
    source 8
    target 33
    preference 27
  ]
  edge [
    source 8
    target 34
    preference 15
  ]
  edge [
    source 8
    target 35
    preference 26
  ]
  edge [
    source 8
    target 36
    preference 1
  ]
  edge [
    source 9
    target 30
    preference 42
  ]
  edge [
    source 9
    target 31
    preference 7
  ]
  edge [
    source 9
    target 32
    preference 2
  ]
  edge [
    source 9
    target 33
    preference 67
  ]
  edge [
    source 9
    target 34
    preference 39
  ]
  edge [
    source 9
    target 35
    preference 54
  ]
  edge [
    source 9
    target 36
    preference 1
  ]
  edge [
    source 10
    target 30
    preference 58
  ]
  edge [
    source 10
    target 31
    preference 62
  ]
  edge [
    source 10
    target 32
    preference 51
  ]
  edge [
    source 10
    target 33
    preference 66
  ]
  edge [
    source 10
    target 34
    preference 16
  ]
  edge [
    source 10
    target 35
    preference 18
  ]
  edge [
    source 10
    target 36
    preference 1
  ]
  edge [
    source 11
    target 30
    preference 22
  ]
  edge [
    source 11
    target 31
    preference 21
  ]
  edge [
    source 11
    target 32
    preference 2
  ]
  edge [
    source 11
    target 33
    preference 14
  ]
  edge [
    source 11
    target 34
    preference 35
  ]
  edge [
    source 11
    target 35
    preference 64
  ]
  edge [
    source 11
    target 36
    preference 1
  ]
  edge [
    source 12
    target 30
    preference 65
  ]
  edge [
    source 12
    target 31
    preference 56
  ]
  edge [
    source 12
    target 32
    preference 59
  ]
  edge [
    source 12
    target 33
    preference 10
  ]
  edge [
    source 12
    target 34
    preference 37
  ]
  edge [
    source 12
    target 35
    preference 69
  ]
  edge [
    source 12
    target 36
    preference 1
  ]
  edge [
    source 13
    target 30
    preference 9
  ]
  edge [
    source 13
    target 31
    preference 36
  ]
  edge [
    source 13
    target 32
    preference 31
  ]
  edge [
    source 13
    target 33
    preference 36
  ]
  edge [
    source 13
    target 34
    preference 16
  ]
  edge [
    source 13
    target 35
    preference 52
  ]
  edge [
    source 13
    target 36
    preference 1
  ]
  edge [
    source 14
    target 30
    preference 37
  ]
  edge [
    source 14
    target 31
    preference 23
  ]
  edge [
    source 14
    target 32
    preference 29
  ]
  edge [
    source 14
    target 33
    preference 43
  ]
  edge [
    source 14
    target 34
    preference 37
  ]
  edge [
    source 14
    target 35
    preference 34
  ]
  edge [
    source 14
    target 36
    preference 1
  ]
  edge [
    source 15
    target 30
    preference 19
  ]
  edge [
    source 15
    target 31
    preference 39
  ]
  edge [
    source 15
    target 32
    preference 9
  ]
  edge [
    source 15
    target 33
    preference 63
  ]
  edge [
    source 15
    target 34
    preference 46
  ]
  edge [
    source 15
    target 35
    preference 62
  ]
  edge [
    source 15
    target 36
    preference 1
  ]
  edge [
    source 16
    target 30
    preference 19
  ]
  edge [
    source 16
    target 31
    preference 45
  ]
  edge [
    source 16
    target 32
    preference 20
  ]
  edge [
    source 16
    target 33
    preference 34
  ]
  edge [
    source 16
    target 34
    preference 54
  ]
  edge [
    source 16
    target 35
    preference 44
  ]
  edge [
    source 16
    target 36
    preference 1
  ]
  edge [
    source 17
    target 30
    preference 21
  ]
  edge [
    source 17
    target 31
    preference 68
  ]
  edge [
    source 17
    target 32
    preference 30
  ]
  edge [
    source 17
    target 33
    preference 41
  ]
  edge [
    source 17
    target 34
    preference 13
  ]
  edge [
    source 17
    target 35
    preference 60
  ]
  edge [
    source 17
    target 36
    preference 1
  ]
  edge [
    source 18
    target 30
    preference 37
  ]
  edge [
    source 18
    target 31
    preference 58
  ]
  edge [
    source 18
    target 32
    preference 26
  ]
  edge [
    source 18
    target 33
    preference 9
  ]
  edge [
    source 18
    target 34
    preference 59
  ]
  edge [
    source 18
    target 35
    preference 62
  ]
  edge [
    source 18
    target 36
    preference 1
  ]
  edge [
    source 19
    target 30
    preference 64
  ]
  edge [
    source 19
    target 31
    preference 71
  ]
  edge [
    source 19
    target 32
    preference 47
  ]
  edge [
    source 19
    target 33
    preference 24
  ]
  edge [
    source 19
    target 34
    preference 39
  ]
  edge [
    source 19
    target 35
    preference 27
  ]
  edge [
    source 19
    target 36
    preference 1
  ]
  edge [
    source 20
    target 30
    preference 72
  ]
  edge [
    source 20
    target 31
    preference 44
  ]
  edge [
    source 20
    target 32
    preference 16
  ]
  edge [
    source 20
    target 33
    preference 43
  ]
  edge [
    source 20
    target 34
    preference 56
  ]
  edge [
    source 20
    target 35
    preference 41
  ]
  edge [
    source 20
    target 36
    preference 1
  ]
  edge [
    source 21
    target 30
    preference 39
  ]
  edge [
    source 21
    target 31
    preference 62
  ]
  edge [
    source 21
    target 32
    preference 55
  ]
  edge [
    source 21
    target 33
    preference 2
  ]
  edge [
    source 21
    target 34
    preference 25
  ]
  edge [
    source 21
    target 35
    preference 24
  ]
  edge [
    source 21
    target 36
    preference 1
  ]
  edge [
    source 22
    target 30
    preference 5
  ]
  edge [
    source 22
    target 31
    preference 69
  ]
  edge [
    source 22
    target 32
    preference 38
  ]
  edge [
    source 22
    target 33
    preference 25
  ]
  edge [
    source 22
    target 34
    preference 17
  ]
  edge [
    source 22
    target 35
    preference 21
  ]
  edge [
    source 22
    target 36
    preference 1
  ]
  edge [
    source 23
    target 30
    preference 54
  ]
  edge [
    source 23
    target 31
    preference 2
  ]
  edge [
    source 23
    target 32
    preference 66
  ]
  edge [
    source 23
    target 33
    preference 54
  ]
  edge [
    source 23
    target 34
    preference 37
  ]
  edge [
    source 23
    target 35
    preference 50
  ]
  edge [
    source 23
    target 36
    preference 1
  ]
  edge [
    source 24
    target 30
    preference 59
  ]
  edge [
    source 24
    target 31
    preference 14
  ]
  edge [
    source 24
    target 32
    preference 71
  ]
  edge [
    source 24
    target 33
    preference 5
  ]
  edge [
    source 24
    target 34
    preference 32
  ]
  edge [
    source 24
    target 35
    preference 60
  ]
  edge [
    source 24
    target 36
    preference 1
  ]
  edge [
    source 25
    target 30
    preference 17
  ]
  edge [
    source 25
    target 31
    preference 48
  ]
  edge [
    source 25
    target 32
    preference 6
  ]
  edge [
    source 25
    target 33
    preference 56
  ]
  edge [
    source 25
    target 34
    preference 70
  ]
  edge [
    source 25
    target 35
    preference 43
  ]
  edge [
    source 25
    target 36
    preference 1
  ]
  edge [
    source 26
    target 30
    preference 64
  ]
  edge [
    source 26
    target 31
    preference 37
  ]
  edge [
    source 26
    target 32
    preference 43
  ]
  edge [
    source 26
    target 33
    preference 24
  ]
  edge [
    source 26
    target 34
    preference 47
  ]
  edge [
    source 26
    target 35
    preference 67
  ]
  edge [
    source 26
    target 36
    preference 1
  ]
  edge [
    source 27
    target 30
    preference 65
  ]
  edge [
    source 27
    target 31
    preference 47
  ]
  edge [
    source 27
    target 32
    preference 9
  ]
  edge [
    source 27
    target 33
    preference 57
  ]
  edge [
    source 27
    target 34
    preference 6
  ]
  edge [
    source 27
    target 35
    preference 65
  ]
  edge [
    source 27
    target 36
    preference 1
  ]
  edge [
    source 28
    target 30
    preference 23
  ]
  edge [
    source 28
    target 31
    preference 50
  ]
  edge [
    source 28
    target 32
    preference 23
  ]
  edge [
    source 28
    target 33
    preference 73
  ]
  edge [
    source 28
    target 34
    preference 72
  ]
  edge [
    source 28
    target 35
    preference 49
  ]
  edge [
    source 28
    target 36
    preference 1
  ]
  edge [
    source 29
    target 30
    preference 35
  ]
  edge [
    source 29
    target 31
    preference 47
  ]
  edge [
    source 29
    target 32
    preference 38
  ]
  edge [
    source 29
    target 33
    preference 16
  ]
  edge [
    source 29
    target 34
    preference 40
  ]
  edge [
    source 29
    target 35
    preference 6
  ]
  edge [
    source 29
    target 36
    preference 1
  ]
  edge [
    source 30
    target 0
    preference 39
  ]
  edge [
    source 30
    target 1
    preference 5
  ]
  edge [
    source 30
    target 2
    preference 29
  ]
  edge [
    source 30
    target 3
    preference 61
  ]
  edge [
    source 30
    target 4
    preference 52
  ]
  edge [
    source 30
    target 5
    preference 19
  ]
  edge [
    source 30
    target 6
    preference 46
  ]
  edge [
    source 30
    target 7
    preference 32
  ]
  edge [
    source 30
    target 8
    preference 36
  ]
  edge [
    source 30
    target 9
    preference 15
  ]
  edge [
    source 30
    target 10
    preference 2
  ]
  edge [
    source 30
    target 11
    preference 59
  ]
  edge [
    source 30
    target 12
    preference 11
  ]
  edge [
    source 30
    target 13
    preference 67
  ]
  edge [
    source 30
    target 14
    preference 8
  ]
  edge [
    source 30
    target 15
    preference 68
  ]
  edge [
    source 30
    target 16
    preference 44
  ]
  edge [
    source 30
    target 17
    preference 18
  ]
  edge [
    source 30
    target 18
    preference 17
  ]
  edge [
    source 30
    target 19
    preference 15
  ]
  edge [
    source 30
    target 20
    preference 50
  ]
  edge [
    source 30
    target 21
    preference 41
  ]
  edge [
    source 30
    target 22
    preference 4
  ]
  edge [
    source 30
    target 23
    preference 64
  ]
  edge [
    source 30
    target 24
    preference 58
  ]
  edge [
    source 30
    target 25
    preference 68
  ]
  edge [
    source 30
    target 26
    preference 11
  ]
  edge [
    source 30
    target 27
    preference 21
  ]
  edge [
    source 30
    target 28
    preference 44
  ]
  edge [
    source 30
    target 29
    preference 44
  ]
  edge [
    source 31
    target 0
    preference 55
  ]
  edge [
    source 31
    target 1
    preference 66
  ]
  edge [
    source 31
    target 2
    preference 19
  ]
  edge [
    source 31
    target 3
    preference 36
  ]
  edge [
    source 31
    target 4
    preference 47
  ]
  edge [
    source 31
    target 5
    preference 46
  ]
  edge [
    source 31
    target 6
    preference 59
  ]
  edge [
    source 31
    target 7
    preference 45
  ]
  edge [
    source 31
    target 8
    preference 50
  ]
  edge [
    source 31
    target 9
    preference 51
  ]
  edge [
    source 31
    target 10
    preference 66
  ]
  edge [
    source 31
    target 11
    preference 56
  ]
  edge [
    source 31
    target 12
    preference 69
  ]
  edge [
    source 31
    target 13
    preference 40
  ]
  edge [
    source 31
    target 14
    preference 19
  ]
  edge [
    source 31
    target 15
    preference 30
  ]
  edge [
    source 31
    target 16
    preference 38
  ]
  edge [
    source 31
    target 17
    preference 15
  ]
  edge [
    source 31
    target 18
    preference 2
  ]
  edge [
    source 31
    target 19
    preference 59
  ]
  edge [
    source 31
    target 20
    preference 13
  ]
  edge [
    source 31
    target 21
    preference 66
  ]
  edge [
    source 31
    target 22
    preference 25
  ]
  edge [
    source 31
    target 23
    preference 24
  ]
  edge [
    source 31
    target 24
    preference 57
  ]
  edge [
    source 31
    target 25
    preference 19
  ]
  edge [
    source 31
    target 26
    preference 43
  ]
  edge [
    source 31
    target 27
    preference 21
  ]
  edge [
    source 31
    target 28
    preference 33
  ]
  edge [
    source 31
    target 29
    preference 69
  ]
  edge [
    source 32
    target 0
    preference 42
  ]
  edge [
    source 32
    target 1
    preference 20
  ]
  edge [
    source 32
    target 2
    preference 35
  ]
  edge [
    source 32
    target 3
    preference 73
  ]
  edge [
    source 32
    target 4
    preference 11
  ]
  edge [
    source 32
    target 5
    preference 53
  ]
  edge [
    source 32
    target 6
    preference 72
  ]
  edge [
    source 32
    target 7
    preference 12
  ]
  edge [
    source 32
    target 8
    preference 37
  ]
  edge [
    source 32
    target 9
    preference 15
  ]
  edge [
    source 32
    target 10
    preference 54
  ]
  edge [
    source 32
    target 11
    preference 58
  ]
  edge [
    source 32
    target 12
    preference 40
  ]
  edge [
    source 32
    target 13
    preference 31
  ]
  edge [
    source 32
    target 14
    preference 22
  ]
  edge [
    source 32
    target 15
    preference 38
  ]
  edge [
    source 32
    target 16
    preference 47
  ]
  edge [
    source 32
    target 17
    preference 38
  ]
  edge [
    source 32
    target 18
    preference 56
  ]
  edge [
    source 32
    target 19
    preference 26
  ]
  edge [
    source 32
    target 20
    preference 35
  ]
  edge [
    source 32
    target 21
    preference 4
  ]
  edge [
    source 32
    target 22
    preference 5
  ]
  edge [
    source 32
    target 23
    preference 41
  ]
  edge [
    source 32
    target 24
    preference 2
  ]
  edge [
    source 32
    target 25
    preference 25
  ]
  edge [
    source 32
    target 26
    preference 6
  ]
  edge [
    source 32
    target 27
    preference 73
  ]
  edge [
    source 32
    target 28
    preference 8
  ]
  edge [
    source 32
    target 29
    preference 74
  ]
  edge [
    source 33
    target 0
    preference 35
  ]
  edge [
    source 33
    target 1
    preference 36
  ]
  edge [
    source 33
    target 2
    preference 42
  ]
  edge [
    source 33
    target 3
    preference 48
  ]
  edge [
    source 33
    target 4
    preference 30
  ]
  edge [
    source 33
    target 5
    preference 57
  ]
  edge [
    source 33
    target 6
    preference 37
  ]
  edge [
    source 33
    target 7
    preference 74
  ]
  edge [
    source 33
    target 8
    preference 40
  ]
  edge [
    source 33
    target 9
    preference 62
  ]
  edge [
    source 33
    target 10
    preference 22
  ]
  edge [
    source 33
    target 11
    preference 65
  ]
  edge [
    source 33
    target 12
    preference 9
  ]
  edge [
    source 33
    target 13
    preference 35
  ]
  edge [
    source 33
    target 14
    preference 32
  ]
  edge [
    source 33
    target 15
    preference 19
  ]
  edge [
    source 33
    target 16
    preference 4
  ]
  edge [
    source 33
    target 17
    preference 35
  ]
  edge [
    source 33
    target 18
    preference 35
  ]
  edge [
    source 33
    target 19
    preference 27
  ]
  edge [
    source 33
    target 20
    preference 41
  ]
  edge [
    source 33
    target 21
    preference 45
  ]
  edge [
    source 33
    target 22
    preference 42
  ]
  edge [
    source 33
    target 23
    preference 9
  ]
  edge [
    source 33
    target 24
    preference 54
  ]
  edge [
    source 33
    target 25
    preference 42
  ]
  edge [
    source 33
    target 26
    preference 11
  ]
  edge [
    source 33
    target 27
    preference 25
  ]
  edge [
    source 33
    target 28
    preference 54
  ]
  edge [
    source 33
    target 29
    preference 14
  ]
  edge [
    source 34
    target 0
    preference 51
  ]
  edge [
    source 34
    target 1
    preference 49
  ]
  edge [
    source 34
    target 2
    preference 5
  ]
  edge [
    source 34
    target 3
    preference 38
  ]
  edge [
    source 34
    target 4
    preference 53
  ]
  edge [
    source 34
    target 5
    preference 11
  ]
  edge [
    source 34
    target 6
    preference 68
  ]
  edge [
    source 34
    target 7
    preference 74
  ]
  edge [
    source 34
    target 8
    preference 16
  ]
  edge [
    source 34
    target 9
    preference 51
  ]
  edge [
    source 34
    target 10
    preference 30
  ]
  edge [
    source 34
    target 11
    preference 2
  ]
  edge [
    source 34
    target 12
    preference 25
  ]
  edge [
    source 34
    target 13
    preference 43
  ]
  edge [
    source 34
    target 14
    preference 73
  ]
  edge [
    source 34
    target 15
    preference 37
  ]
  edge [
    source 34
    target 16
    preference 30
  ]
  edge [
    source 34
    target 17
    preference 62
  ]
  edge [
    source 34
    target 18
    preference 3
  ]
  edge [
    source 34
    target 19
    preference 64
  ]
  edge [
    source 34
    target 20
    preference 26
  ]
  edge [
    source 34
    target 21
    preference 70
  ]
  edge [
    source 34
    target 22
    preference 61
  ]
  edge [
    source 34
    target 23
    preference 26
  ]
  edge [
    source 34
    target 24
    preference 45
  ]
  edge [
    source 34
    target 25
    preference 49
  ]
  edge [
    source 34
    target 26
    preference 31
  ]
  edge [
    source 34
    target 27
    preference 51
  ]
  edge [
    source 34
    target 28
    preference 5
  ]
  edge [
    source 34
    target 29
    preference 7
  ]
  edge [
    source 35
    target 0
    preference 37
  ]
  edge [
    source 35
    target 1
    preference 45
  ]
  edge [
    source 35
    target 2
    preference 64
  ]
  edge [
    source 35
    target 3
    preference 29
  ]
  edge [
    source 35
    target 4
    preference 4
  ]
  edge [
    source 35
    target 5
    preference 19
  ]
  edge [
    source 35
    target 6
    preference 28
  ]
  edge [
    source 35
    target 7
    preference 12
  ]
  edge [
    source 35
    target 8
    preference 60
  ]
  edge [
    source 35
    target 9
    preference 29
  ]
  edge [
    source 35
    target 10
    preference 55
  ]
  edge [
    source 35
    target 11
    preference 46
  ]
  edge [
    source 35
    target 12
    preference 28
  ]
  edge [
    source 35
    target 13
    preference 30
  ]
  edge [
    source 35
    target 14
    preference 22
  ]
  edge [
    source 35
    target 15
    preference 14
  ]
  edge [
    source 35
    target 16
    preference 73
  ]
  edge [
    source 35
    target 17
    preference 41
  ]
  edge [
    source 35
    target 18
    preference 58
  ]
  edge [
    source 35
    target 19
    preference 47
  ]
  edge [
    source 35
    target 20
    preference 26
  ]
  edge [
    source 35
    target 21
    preference 51
  ]
  edge [
    source 35
    target 22
    preference 49
  ]
  edge [
    source 35
    target 23
    preference 7
  ]
  edge [
    source 35
    target 24
    preference 41
  ]
  edge [
    source 35
    target 25
    preference 70
  ]
  edge [
    source 35
    target 26
    preference 23
  ]
  edge [
    source 35
    target 27
    preference 27
  ]
  edge [
    source 35
    target 28
    preference 20
  ]
  edge [
    source 35
    target 29
    preference 41
  ]
  edge [
    source 36
    target 0
    preference 1
  ]
  edge [
    source 36
    target 1
    preference 1
  ]
  edge [
    source 36
    target 2
    preference 1
  ]
  edge [
    source 36
    target 3
    preference 1
  ]
  edge [
    source 36
    target 4
    preference 1
  ]
  edge [
    source 36
    target 5
    preference 1
  ]
  edge [
    source 36
    target 6
    preference 1
  ]
  edge [
    source 36
    target 7
    preference 1
  ]
  edge [
    source 36
    target 8
    preference 1
  ]
  edge [
    source 36
    target 9
    preference 1
  ]
  edge [
    source 36
    target 10
    preference 1
  ]
  edge [
    source 36
    target 11
    preference 1
  ]
  edge [
    source 36
    target 12
    preference 1
  ]
  edge [
    source 36
    target 13
    preference 1
  ]
  edge [
    source 36
    target 14
    preference 1
  ]
  edge [
    source 36
    target 15
    preference 1
  ]
  edge [
    source 36
    target 16
    preference 1
  ]
  edge [
    source 36
    target 17
    preference 1
  ]
  edge [
    source 36
    target 18
    preference 1
  ]
  edge [
    source 36
    target 19
    preference 1
  ]
  edge [
    source 36
    target 20
    preference 1
  ]
  edge [
    source 36
    target 21
    preference 1
  ]
  edge [
    source 36
    target 22
    preference 1
  ]
  edge [
    source 36
    target 23
    preference 1
  ]
  edge [
    source 36
    target 24
    preference 1
  ]
  edge [
    source 36
    target 25
    preference 1
  ]
  edge [
    source 36
    target 26
    preference 1
  ]
  edge [
    source 36
    target 27
    preference 1
  ]
  edge [
    source 36
    target 28
    preference 1
  ]
  edge [
    source 36
    target 29
    preference 1
  ]
]
