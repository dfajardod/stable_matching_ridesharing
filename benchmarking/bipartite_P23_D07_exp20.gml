graph [
  directed 1
  name "complete_bipartite_graph(23,8)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 16
    label "16"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 17
    label "17"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 18
    label "18"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 19
    label "19"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 20
    label "20"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 21
    label "21"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 22
    label "22"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 23
    label "23"
    bipartite 1
    availableSeats 3
  ]
  node [
    id 24
    label "24"
    bipartite 1
    availableSeats 3
  ]
  node [
    id 25
    label "25"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 26
    label "26"
    bipartite 1
    availableSeats 2
  ]
  node [
    id 27
    label "27"
    bipartite 1
    availableSeats 3
  ]
  node [
    id 28
    label "28"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 29
    label "29"
    bipartite 1
    availableSeats 3
  ]
  node [
    id 30
    label "30"
    bipartite 1
    availableSeats 1
    waitingList 1
  ]
  edge [
    source 0
    target 23
    preference 61
  ]
  edge [
    source 0
    target 24
    preference 20
  ]
  edge [
    source 0
    target 25
    preference 53
  ]
  edge [
    source 0
    target 26
    preference 8
  ]
  edge [
    source 0
    target 27
    preference 5
  ]
  edge [
    source 0
    target 28
    preference 8
  ]
  edge [
    source 0
    target 29
    preference 38
  ]
  edge [
    source 0
    target 30
    preference 1
  ]
  edge [
    source 1
    target 23
    preference 50
  ]
  edge [
    source 1
    target 24
    preference 16
  ]
  edge [
    source 1
    target 25
    preference 42
  ]
  edge [
    source 1
    target 26
    preference 58
  ]
  edge [
    source 1
    target 27
    preference 60
  ]
  edge [
    source 1
    target 28
    preference 53
  ]
  edge [
    source 1
    target 29
    preference 12
  ]
  edge [
    source 1
    target 30
    preference 1
  ]
  edge [
    source 2
    target 23
    preference 20
  ]
  edge [
    source 2
    target 24
    preference 6
  ]
  edge [
    source 2
    target 25
    preference 31
  ]
  edge [
    source 2
    target 26
    preference 62
  ]
  edge [
    source 2
    target 27
    preference 32
  ]
  edge [
    source 2
    target 28
    preference 37
  ]
  edge [
    source 2
    target 29
    preference 20
  ]
  edge [
    source 2
    target 30
    preference 1
  ]
  edge [
    source 3
    target 23
    preference 12
  ]
  edge [
    source 3
    target 24
    preference 39
  ]
  edge [
    source 3
    target 25
    preference 16
  ]
  edge [
    source 3
    target 26
    preference 51
  ]
  edge [
    source 3
    target 27
    preference 2
  ]
  edge [
    source 3
    target 28
    preference 12
  ]
  edge [
    source 3
    target 29
    preference 47
  ]
  edge [
    source 3
    target 30
    preference 1
  ]
  edge [
    source 4
    target 23
    preference 22
  ]
  edge [
    source 4
    target 24
    preference 60
  ]
  edge [
    source 4
    target 25
    preference 20
  ]
  edge [
    source 4
    target 26
    preference 53
  ]
  edge [
    source 4
    target 27
    preference 29
  ]
  edge [
    source 4
    target 28
    preference 46
  ]
  edge [
    source 4
    target 29
    preference 49
  ]
  edge [
    source 4
    target 30
    preference 1
  ]
  edge [
    source 5
    target 23
    preference 54
  ]
  edge [
    source 5
    target 24
    preference 26
  ]
  edge [
    source 5
    target 25
    preference 56
  ]
  edge [
    source 5
    target 26
    preference 49
  ]
  edge [
    source 5
    target 27
    preference 51
  ]
  edge [
    source 5
    target 28
    preference 37
  ]
  edge [
    source 5
    target 29
    preference 28
  ]
  edge [
    source 5
    target 30
    preference 1
  ]
  edge [
    source 6
    target 23
    preference 38
  ]
  edge [
    source 6
    target 24
    preference 55
  ]
  edge [
    source 6
    target 25
    preference 42
  ]
  edge [
    source 6
    target 26
    preference 39
  ]
  edge [
    source 6
    target 27
    preference 13
  ]
  edge [
    source 6
    target 28
    preference 2
  ]
  edge [
    source 6
    target 29
    preference 19
  ]
  edge [
    source 6
    target 30
    preference 1
  ]
  edge [
    source 7
    target 23
    preference 35
  ]
  edge [
    source 7
    target 24
    preference 16
  ]
  edge [
    source 7
    target 25
    preference 5
  ]
  edge [
    source 7
    target 26
    preference 13
  ]
  edge [
    source 7
    target 27
    preference 15
  ]
  edge [
    source 7
    target 28
    preference 25
  ]
  edge [
    source 7
    target 29
    preference 39
  ]
  edge [
    source 7
    target 30
    preference 1
  ]
  edge [
    source 8
    target 23
    preference 62
  ]
  edge [
    source 8
    target 24
    preference 48
  ]
  edge [
    source 8
    target 25
    preference 37
  ]
  edge [
    source 8
    target 26
    preference 9
  ]
  edge [
    source 8
    target 27
    preference 29
  ]
  edge [
    source 8
    target 28
    preference 36
  ]
  edge [
    source 8
    target 29
    preference 39
  ]
  edge [
    source 8
    target 30
    preference 1
  ]
  edge [
    source 9
    target 23
    preference 14
  ]
  edge [
    source 9
    target 24
    preference 22
  ]
  edge [
    source 9
    target 25
    preference 35
  ]
  edge [
    source 9
    target 26
    preference 50
  ]
  edge [
    source 9
    target 27
    preference 45
  ]
  edge [
    source 9
    target 28
    preference 56
  ]
  edge [
    source 9
    target 29
    preference 52
  ]
  edge [
    source 9
    target 30
    preference 1
  ]
  edge [
    source 10
    target 23
    preference 20
  ]
  edge [
    source 10
    target 24
    preference 56
  ]
  edge [
    source 10
    target 25
    preference 27
  ]
  edge [
    source 10
    target 26
    preference 35
  ]
  edge [
    source 10
    target 27
    preference 47
  ]
  edge [
    source 10
    target 28
    preference 52
  ]
  edge [
    source 10
    target 29
    preference 24
  ]
  edge [
    source 10
    target 30
    preference 1
  ]
  edge [
    source 11
    target 23
    preference 34
  ]
  edge [
    source 11
    target 24
    preference 17
  ]
  edge [
    source 11
    target 25
    preference 32
  ]
  edge [
    source 11
    target 26
    preference 35
  ]
  edge [
    source 11
    target 27
    preference 26
  ]
  edge [
    source 11
    target 28
    preference 9
  ]
  edge [
    source 11
    target 29
    preference 41
  ]
  edge [
    source 11
    target 30
    preference 1
  ]
  edge [
    source 12
    target 23
    preference 24
  ]
  edge [
    source 12
    target 24
    preference 13
  ]
  edge [
    source 12
    target 25
    preference 44
  ]
  edge [
    source 12
    target 26
    preference 2
  ]
  edge [
    source 12
    target 27
    preference 47
  ]
  edge [
    source 12
    target 28
    preference 50
  ]
  edge [
    source 12
    target 29
    preference 34
  ]
  edge [
    source 12
    target 30
    preference 1
  ]
  edge [
    source 13
    target 23
    preference 57
  ]
  edge [
    source 13
    target 24
    preference 11
  ]
  edge [
    source 13
    target 25
    preference 21
  ]
  edge [
    source 13
    target 26
    preference 57
  ]
  edge [
    source 13
    target 27
    preference 50
  ]
  edge [
    source 13
    target 28
    preference 18
  ]
  edge [
    source 13
    target 29
    preference 10
  ]
  edge [
    source 13
    target 30
    preference 1
  ]
  edge [
    source 14
    target 23
    preference 26
  ]
  edge [
    source 14
    target 24
    preference 62
  ]
  edge [
    source 14
    target 25
    preference 8
  ]
  edge [
    source 14
    target 26
    preference 23
  ]
  edge [
    source 14
    target 27
    preference 36
  ]
  edge [
    source 14
    target 28
    preference 8
  ]
  edge [
    source 14
    target 29
    preference 18
  ]
  edge [
    source 14
    target 30
    preference 1
  ]
  edge [
    source 15
    target 23
    preference 4
  ]
  edge [
    source 15
    target 24
    preference 60
  ]
  edge [
    source 15
    target 25
    preference 46
  ]
  edge [
    source 15
    target 26
    preference 50
  ]
  edge [
    source 15
    target 27
    preference 43
  ]
  edge [
    source 15
    target 28
    preference 58
  ]
  edge [
    source 15
    target 29
    preference 58
  ]
  edge [
    source 15
    target 30
    preference 1
  ]
  edge [
    source 16
    target 23
    preference 48
  ]
  edge [
    source 16
    target 24
    preference 43
  ]
  edge [
    source 16
    target 25
    preference 8
  ]
  edge [
    source 16
    target 26
    preference 61
  ]
  edge [
    source 16
    target 27
    preference 28
  ]
  edge [
    source 16
    target 28
    preference 20
  ]
  edge [
    source 16
    target 29
    preference 31
  ]
  edge [
    source 16
    target 30
    preference 1
  ]
  edge [
    source 17
    target 23
    preference 26
  ]
  edge [
    source 17
    target 24
    preference 21
  ]
  edge [
    source 17
    target 25
    preference 58
  ]
  edge [
    source 17
    target 26
    preference 53
  ]
  edge [
    source 17
    target 27
    preference 3
  ]
  edge [
    source 17
    target 28
    preference 24
  ]
  edge [
    source 17
    target 29
    preference 27
  ]
  edge [
    source 17
    target 30
    preference 1
  ]
  edge [
    source 18
    target 23
    preference 5
  ]
  edge [
    source 18
    target 24
    preference 11
  ]
  edge [
    source 18
    target 25
    preference 53
  ]
  edge [
    source 18
    target 26
    preference 7
  ]
  edge [
    source 18
    target 27
    preference 59
  ]
  edge [
    source 18
    target 28
    preference 16
  ]
  edge [
    source 18
    target 29
    preference 43
  ]
  edge [
    source 18
    target 30
    preference 1
  ]
  edge [
    source 19
    target 23
    preference 41
  ]
  edge [
    source 19
    target 24
    preference 33
  ]
  edge [
    source 19
    target 25
    preference 29
  ]
  edge [
    source 19
    target 26
    preference 60
  ]
  edge [
    source 19
    target 27
    preference 44
  ]
  edge [
    source 19
    target 28
    preference 52
  ]
  edge [
    source 19
    target 29
    preference 28
  ]
  edge [
    source 19
    target 30
    preference 1
  ]
  edge [
    source 20
    target 23
    preference 12
  ]
  edge [
    source 20
    target 24
    preference 36
  ]
  edge [
    source 20
    target 25
    preference 56
  ]
  edge [
    source 20
    target 26
    preference 20
  ]
  edge [
    source 20
    target 27
    preference 57
  ]
  edge [
    source 20
    target 28
    preference 52
  ]
  edge [
    source 20
    target 29
    preference 5
  ]
  edge [
    source 20
    target 30
    preference 1
  ]
  edge [
    source 21
    target 23
    preference 53
  ]
  edge [
    source 21
    target 24
    preference 12
  ]
  edge [
    source 21
    target 25
    preference 18
  ]
  edge [
    source 21
    target 26
    preference 59
  ]
  edge [
    source 21
    target 27
    preference 51
  ]
  edge [
    source 21
    target 28
    preference 7
  ]
  edge [
    source 21
    target 29
    preference 16
  ]
  edge [
    source 21
    target 30
    preference 1
  ]
  edge [
    source 22
    target 23
    preference 38
  ]
  edge [
    source 22
    target 24
    preference 36
  ]
  edge [
    source 22
    target 25
    preference 46
  ]
  edge [
    source 22
    target 26
    preference 56
  ]
  edge [
    source 22
    target 27
    preference 29
  ]
  edge [
    source 22
    target 28
    preference 39
  ]
  edge [
    source 22
    target 29
    preference 16
  ]
  edge [
    source 22
    target 30
    preference 1
  ]
  edge [
    source 23
    target 0
    preference 21
  ]
  edge [
    source 23
    target 1
    preference 48
  ]
  edge [
    source 23
    target 2
    preference 9
  ]
  edge [
    source 23
    target 3
    preference 2
  ]
  edge [
    source 23
    target 4
    preference 11
  ]
  edge [
    source 23
    target 5
    preference 21
  ]
  edge [
    source 23
    target 6
    preference 7
  ]
  edge [
    source 23
    target 7
    preference 51
  ]
  edge [
    source 23
    target 8
    preference 62
  ]
  edge [
    source 23
    target 9
    preference 19
  ]
  edge [
    source 23
    target 10
    preference 10
  ]
  edge [
    source 23
    target 11
    preference 54
  ]
  edge [
    source 23
    target 12
    preference 9
  ]
  edge [
    source 23
    target 13
    preference 32
  ]
  edge [
    source 23
    target 14
    preference 40
  ]
  edge [
    source 23
    target 15
    preference 21
  ]
  edge [
    source 23
    target 16
    preference 45
  ]
  edge [
    source 23
    target 17
    preference 6
  ]
  edge [
    source 23
    target 18
    preference 20
  ]
  edge [
    source 23
    target 19
    preference 26
  ]
  edge [
    source 23
    target 20
    preference 6
  ]
  edge [
    source 23
    target 21
    preference 56
  ]
  edge [
    source 23
    target 22
    preference 15
  ]
  edge [
    source 24
    target 0
    preference 50
  ]
  edge [
    source 24
    target 1
    preference 9
  ]
  edge [
    source 24
    target 2
    preference 2
  ]
  edge [
    source 24
    target 3
    preference 45
  ]
  edge [
    source 24
    target 4
    preference 50
  ]
  edge [
    source 24
    target 5
    preference 35
  ]
  edge [
    source 24
    target 6
    preference 38
  ]
  edge [
    source 24
    target 7
    preference 2
  ]
  edge [
    source 24
    target 8
    preference 16
  ]
  edge [
    source 24
    target 9
    preference 30
  ]
  edge [
    source 24
    target 10
    preference 15
  ]
  edge [
    source 24
    target 11
    preference 55
  ]
  edge [
    source 24
    target 12
    preference 46
  ]
  edge [
    source 24
    target 13
    preference 47
  ]
  edge [
    source 24
    target 14
    preference 35
  ]
  edge [
    source 24
    target 15
    preference 25
  ]
  edge [
    source 24
    target 16
    preference 23
  ]
  edge [
    source 24
    target 17
    preference 2
  ]
  edge [
    source 24
    target 18
    preference 37
  ]
  edge [
    source 24
    target 19
    preference 4
  ]
  edge [
    source 24
    target 20
    preference 33
  ]
  edge [
    source 24
    target 21
    preference 50
  ]
  edge [
    source 24
    target 22
    preference 41
  ]
  edge [
    source 25
    target 0
    preference 15
  ]
  edge [
    source 25
    target 1
    preference 37
  ]
  edge [
    source 25
    target 2
    preference 2
  ]
  edge [
    source 25
    target 3
    preference 27
  ]
  edge [
    source 25
    target 4
    preference 36
  ]
  edge [
    source 25
    target 5
    preference 17
  ]
  edge [
    source 25
    target 6
    preference 5
  ]
  edge [
    source 25
    target 7
    preference 27
  ]
  edge [
    source 25
    target 8
    preference 17
  ]
  edge [
    source 25
    target 9
    preference 9
  ]
  edge [
    source 25
    target 10
    preference 21
  ]
  edge [
    source 25
    target 11
    preference 2
  ]
  edge [
    source 25
    target 12
    preference 55
  ]
  edge [
    source 25
    target 13
    preference 14
  ]
  edge [
    source 25
    target 14
    preference 38
  ]
  edge [
    source 25
    target 15
    preference 46
  ]
  edge [
    source 25
    target 16
    preference 62
  ]
  edge [
    source 25
    target 17
    preference 29
  ]
  edge [
    source 25
    target 18
    preference 17
  ]
  edge [
    source 25
    target 19
    preference 18
  ]
  edge [
    source 25
    target 20
    preference 5
  ]
  edge [
    source 25
    target 21
    preference 53
  ]
  edge [
    source 25
    target 22
    preference 54
  ]
  edge [
    source 26
    target 0
    preference 2
  ]
  edge [
    source 26
    target 1
    preference 41
  ]
  edge [
    source 26
    target 2
    preference 55
  ]
  edge [
    source 26
    target 3
    preference 45
  ]
  edge [
    source 26
    target 4
    preference 61
  ]
  edge [
    source 26
    target 5
    preference 39
  ]
  edge [
    source 26
    target 6
    preference 15
  ]
  edge [
    source 26
    target 7
    preference 13
  ]
  edge [
    source 26
    target 8
    preference 19
  ]
  edge [
    source 26
    target 9
    preference 10
  ]
  edge [
    source 26
    target 10
    preference 41
  ]
  edge [
    source 26
    target 11
    preference 23
  ]
  edge [
    source 26
    target 12
    preference 10
  ]
  edge [
    source 26
    target 13
    preference 7
  ]
  edge [
    source 26
    target 14
    preference 28
  ]
  edge [
    source 26
    target 15
    preference 9
  ]
  edge [
    source 26
    target 16
    preference 14
  ]
  edge [
    source 26
    target 17
    preference 24
  ]
  edge [
    source 26
    target 18
    preference 54
  ]
  edge [
    source 26
    target 19
    preference 10
  ]
  edge [
    source 26
    target 20
    preference 6
  ]
  edge [
    source 26
    target 21
    preference 54
  ]
  edge [
    source 26
    target 22
    preference 49
  ]
  edge [
    source 27
    target 0
    preference 46
  ]
  edge [
    source 27
    target 1
    preference 43
  ]
  edge [
    source 27
    target 2
    preference 54
  ]
  edge [
    source 27
    target 3
    preference 29
  ]
  edge [
    source 27
    target 4
    preference 45
  ]
  edge [
    source 27
    target 5
    preference 57
  ]
  edge [
    source 27
    target 6
    preference 62
  ]
  edge [
    source 27
    target 7
    preference 5
  ]
  edge [
    source 27
    target 8
    preference 32
  ]
  edge [
    source 27
    target 9
    preference 40
  ]
  edge [
    source 27
    target 10
    preference 30
  ]
  edge [
    source 27
    target 11
    preference 14
  ]
  edge [
    source 27
    target 12
    preference 5
  ]
  edge [
    source 27
    target 13
    preference 58
  ]
  edge [
    source 27
    target 14
    preference 22
  ]
  edge [
    source 27
    target 15
    preference 48
  ]
  edge [
    source 27
    target 16
    preference 39
  ]
  edge [
    source 27
    target 17
    preference 12
  ]
  edge [
    source 27
    target 18
    preference 19
  ]
  edge [
    source 27
    target 19
    preference 16
  ]
  edge [
    source 27
    target 20
    preference 30
  ]
  edge [
    source 27
    target 21
    preference 17
  ]
  edge [
    source 27
    target 22
    preference 34
  ]
  edge [
    source 28
    target 0
    preference 47
  ]
  edge [
    source 28
    target 1
    preference 11
  ]
  edge [
    source 28
    target 2
    preference 37
  ]
  edge [
    source 28
    target 3
    preference 62
  ]
  edge [
    source 28
    target 4
    preference 24
  ]
  edge [
    source 28
    target 5
    preference 15
  ]
  edge [
    source 28
    target 6
    preference 58
  ]
  edge [
    source 28
    target 7
    preference 44
  ]
  edge [
    source 28
    target 8
    preference 18
  ]
  edge [
    source 28
    target 9
    preference 26
  ]
  edge [
    source 28
    target 10
    preference 22
  ]
  edge [
    source 28
    target 11
    preference 14
  ]
  edge [
    source 28
    target 12
    preference 29
  ]
  edge [
    source 28
    target 13
    preference 13
  ]
  edge [
    source 28
    target 14
    preference 18
  ]
  edge [
    source 28
    target 15
    preference 60
  ]
  edge [
    source 28
    target 16
    preference 48
  ]
  edge [
    source 28
    target 17
    preference 37
  ]
  edge [
    source 28
    target 18
    preference 11
  ]
  edge [
    source 28
    target 19
    preference 55
  ]
  edge [
    source 28
    target 20
    preference 38
  ]
  edge [
    source 28
    target 21
    preference 37
  ]
  edge [
    source 28
    target 22
    preference 45
  ]
  edge [
    source 29
    target 0
    preference 8
  ]
  edge [
    source 29
    target 1
    preference 34
  ]
  edge [
    source 29
    target 2
    preference 36
  ]
  edge [
    source 29
    target 3
    preference 6
  ]
  edge [
    source 29
    target 4
    preference 62
  ]
  edge [
    source 29
    target 5
    preference 32
  ]
  edge [
    source 29
    target 6
    preference 22
  ]
  edge [
    source 29
    target 7
    preference 15
  ]
  edge [
    source 29
    target 8
    preference 18
  ]
  edge [
    source 29
    target 9
    preference 4
  ]
  edge [
    source 29
    target 10
    preference 62
  ]
  edge [
    source 29
    target 11
    preference 35
  ]
  edge [
    source 29
    target 12
    preference 8
  ]
  edge [
    source 29
    target 13
    preference 9
  ]
  edge [
    source 29
    target 14
    preference 5
  ]
  edge [
    source 29
    target 15
    preference 15
  ]
  edge [
    source 29
    target 16
    preference 9
  ]
  edge [
    source 29
    target 17
    preference 16
  ]
  edge [
    source 29
    target 18
    preference 54
  ]
  edge [
    source 29
    target 19
    preference 54
  ]
  edge [
    source 29
    target 20
    preference 20
  ]
  edge [
    source 29
    target 21
    preference 45
  ]
  edge [
    source 29
    target 22
    preference 6
  ]
  edge [
    source 30
    target 0
    preference 1
  ]
  edge [
    source 30
    target 1
    preference 1
  ]
  edge [
    source 30
    target 2
    preference 1
  ]
  edge [
    source 30
    target 3
    preference 1
  ]
  edge [
    source 30
    target 4
    preference 1
  ]
  edge [
    source 30
    target 5
    preference 1
  ]
  edge [
    source 30
    target 6
    preference 1
  ]
  edge [
    source 30
    target 7
    preference 1
  ]
  edge [
    source 30
    target 8
    preference 1
  ]
  edge [
    source 30
    target 9
    preference 1
  ]
  edge [
    source 30
    target 10
    preference 1
  ]
  edge [
    source 30
    target 11
    preference 1
  ]
  edge [
    source 30
    target 12
    preference 1
  ]
  edge [
    source 30
    target 13
    preference 1
  ]
  edge [
    source 30
    target 14
    preference 1
  ]
  edge [
    source 30
    target 15
    preference 1
  ]
  edge [
    source 30
    target 16
    preference 1
  ]
  edge [
    source 30
    target 17
    preference 1
  ]
  edge [
    source 30
    target 18
    preference 1
  ]
  edge [
    source 30
    target 19
    preference 1
  ]
  edge [
    source 30
    target 20
    preference 1
  ]
  edge [
    source 30
    target 21
    preference 1
  ]
  edge [
    source 30
    target 22
    preference 1
  ]
]
