graph [
  directed 1
  name "complete_bipartite_graph(26,8)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 16
    label "16"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 17
    label "17"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 18
    label "18"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 19
    label "19"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 20
    label "20"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 21
    label "21"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 22
    label "22"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 23
    label "23"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 24
    label "24"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 25
    label "25"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 26
    label "26"
    bipartite 1
    availableSeats 2
  ]
  node [
    id 27
    label "27"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 28
    label "28"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 29
    label "29"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 30
    label "30"
    bipartite 1
    availableSeats 3
  ]
  node [
    id 31
    label "31"
    bipartite 1
    availableSeats 2
  ]
  node [
    id 32
    label "32"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 33
    label "33"
    bipartite 1
    availableSeats 3
    waitingList 1
  ]
  edge [
    source 0
    target 26
    preference 48
  ]
  edge [
    source 0
    target 27
    preference 27
  ]
  edge [
    source 0
    target 28
    preference 55
  ]
  edge [
    source 0
    target 29
    preference 22
  ]
  edge [
    source 0
    target 30
    preference 41
  ]
  edge [
    source 0
    target 31
    preference 36
  ]
  edge [
    source 0
    target 32
    preference 68
  ]
  edge [
    source 0
    target 33
    preference 1
  ]
  edge [
    source 1
    target 26
    preference 44
  ]
  edge [
    source 1
    target 27
    preference 11
  ]
  edge [
    source 1
    target 28
    preference 22
  ]
  edge [
    source 1
    target 29
    preference 11
  ]
  edge [
    source 1
    target 30
    preference 62
  ]
  edge [
    source 1
    target 31
    preference 33
  ]
  edge [
    source 1
    target 32
    preference 67
  ]
  edge [
    source 1
    target 33
    preference 1
  ]
  edge [
    source 2
    target 26
    preference 22
  ]
  edge [
    source 2
    target 27
    preference 53
  ]
  edge [
    source 2
    target 28
    preference 64
  ]
  edge [
    source 2
    target 29
    preference 63
  ]
  edge [
    source 2
    target 30
    preference 24
  ]
  edge [
    source 2
    target 31
    preference 23
  ]
  edge [
    source 2
    target 32
    preference 64
  ]
  edge [
    source 2
    target 33
    preference 1
  ]
  edge [
    source 3
    target 26
    preference 20
  ]
  edge [
    source 3
    target 27
    preference 45
  ]
  edge [
    source 3
    target 28
    preference 21
  ]
  edge [
    source 3
    target 29
    preference 24
  ]
  edge [
    source 3
    target 30
    preference 42
  ]
  edge [
    source 3
    target 31
    preference 8
  ]
  edge [
    source 3
    target 32
    preference 15
  ]
  edge [
    source 3
    target 33
    preference 1
  ]
  edge [
    source 4
    target 26
    preference 9
  ]
  edge [
    source 4
    target 27
    preference 45
  ]
  edge [
    source 4
    target 28
    preference 2
  ]
  edge [
    source 4
    target 29
    preference 53
  ]
  edge [
    source 4
    target 30
    preference 7
  ]
  edge [
    source 4
    target 31
    preference 49
  ]
  edge [
    source 4
    target 32
    preference 3
  ]
  edge [
    source 4
    target 33
    preference 1
  ]
  edge [
    source 5
    target 26
    preference 6
  ]
  edge [
    source 5
    target 27
    preference 6
  ]
  edge [
    source 5
    target 28
    preference 7
  ]
  edge [
    source 5
    target 29
    preference 36
  ]
  edge [
    source 5
    target 30
    preference 17
  ]
  edge [
    source 5
    target 31
    preference 25
  ]
  edge [
    source 5
    target 32
    preference 46
  ]
  edge [
    source 5
    target 33
    preference 1
  ]
  edge [
    source 6
    target 26
    preference 29
  ]
  edge [
    source 6
    target 27
    preference 24
  ]
  edge [
    source 6
    target 28
    preference 3
  ]
  edge [
    source 6
    target 29
    preference 49
  ]
  edge [
    source 6
    target 30
    preference 22
  ]
  edge [
    source 6
    target 31
    preference 39
  ]
  edge [
    source 6
    target 32
    preference 43
  ]
  edge [
    source 6
    target 33
    preference 1
  ]
  edge [
    source 7
    target 26
    preference 59
  ]
  edge [
    source 7
    target 27
    preference 25
  ]
  edge [
    source 7
    target 28
    preference 52
  ]
  edge [
    source 7
    target 29
    preference 40
  ]
  edge [
    source 7
    target 30
    preference 45
  ]
  edge [
    source 7
    target 31
    preference 64
  ]
  edge [
    source 7
    target 32
    preference 11
  ]
  edge [
    source 7
    target 33
    preference 1
  ]
  edge [
    source 8
    target 26
    preference 63
  ]
  edge [
    source 8
    target 27
    preference 29
  ]
  edge [
    source 8
    target 28
    preference 49
  ]
  edge [
    source 8
    target 29
    preference 28
  ]
  edge [
    source 8
    target 30
    preference 47
  ]
  edge [
    source 8
    target 31
    preference 42
  ]
  edge [
    source 8
    target 32
    preference 24
  ]
  edge [
    source 8
    target 33
    preference 1
  ]
  edge [
    source 9
    target 26
    preference 16
  ]
  edge [
    source 9
    target 27
    preference 13
  ]
  edge [
    source 9
    target 28
    preference 55
  ]
  edge [
    source 9
    target 29
    preference 51
  ]
  edge [
    source 9
    target 30
    preference 43
  ]
  edge [
    source 9
    target 31
    preference 9
  ]
  edge [
    source 9
    target 32
    preference 63
  ]
  edge [
    source 9
    target 33
    preference 1
  ]
  edge [
    source 10
    target 26
    preference 8
  ]
  edge [
    source 10
    target 27
    preference 10
  ]
  edge [
    source 10
    target 28
    preference 20
  ]
  edge [
    source 10
    target 29
    preference 38
  ]
  edge [
    source 10
    target 30
    preference 50
  ]
  edge [
    source 10
    target 31
    preference 15
  ]
  edge [
    source 10
    target 32
    preference 18
  ]
  edge [
    source 10
    target 33
    preference 1
  ]
  edge [
    source 11
    target 26
    preference 16
  ]
  edge [
    source 11
    target 27
    preference 65
  ]
  edge [
    source 11
    target 28
    preference 24
  ]
  edge [
    source 11
    target 29
    preference 54
  ]
  edge [
    source 11
    target 30
    preference 27
  ]
  edge [
    source 11
    target 31
    preference 63
  ]
  edge [
    source 11
    target 32
    preference 67
  ]
  edge [
    source 11
    target 33
    preference 1
  ]
  edge [
    source 12
    target 26
    preference 67
  ]
  edge [
    source 12
    target 27
    preference 60
  ]
  edge [
    source 12
    target 28
    preference 9
  ]
  edge [
    source 12
    target 29
    preference 23
  ]
  edge [
    source 12
    target 30
    preference 19
  ]
  edge [
    source 12
    target 31
    preference 65
  ]
  edge [
    source 12
    target 32
    preference 38
  ]
  edge [
    source 12
    target 33
    preference 1
  ]
  edge [
    source 13
    target 26
    preference 34
  ]
  edge [
    source 13
    target 27
    preference 37
  ]
  edge [
    source 13
    target 28
    preference 67
  ]
  edge [
    source 13
    target 29
    preference 50
  ]
  edge [
    source 13
    target 30
    preference 8
  ]
  edge [
    source 13
    target 31
    preference 5
  ]
  edge [
    source 13
    target 32
    preference 40
  ]
  edge [
    source 13
    target 33
    preference 1
  ]
  edge [
    source 14
    target 26
    preference 35
  ]
  edge [
    source 14
    target 27
    preference 10
  ]
  edge [
    source 14
    target 28
    preference 65
  ]
  edge [
    source 14
    target 29
    preference 32
  ]
  edge [
    source 14
    target 30
    preference 5
  ]
  edge [
    source 14
    target 31
    preference 61
  ]
  edge [
    source 14
    target 32
    preference 49
  ]
  edge [
    source 14
    target 33
    preference 1
  ]
  edge [
    source 15
    target 26
    preference 64
  ]
  edge [
    source 15
    target 27
    preference 40
  ]
  edge [
    source 15
    target 28
    preference 8
  ]
  edge [
    source 15
    target 29
    preference 7
  ]
  edge [
    source 15
    target 30
    preference 64
  ]
  edge [
    source 15
    target 31
    preference 38
  ]
  edge [
    source 15
    target 32
    preference 12
  ]
  edge [
    source 15
    target 33
    preference 1
  ]
  edge [
    source 16
    target 26
    preference 63
  ]
  edge [
    source 16
    target 27
    preference 67
  ]
  edge [
    source 16
    target 28
    preference 13
  ]
  edge [
    source 16
    target 29
    preference 8
  ]
  edge [
    source 16
    target 30
    preference 34
  ]
  edge [
    source 16
    target 31
    preference 29
  ]
  edge [
    source 16
    target 32
    preference 46
  ]
  edge [
    source 16
    target 33
    preference 1
  ]
  edge [
    source 17
    target 26
    preference 38
  ]
  edge [
    source 17
    target 27
    preference 59
  ]
  edge [
    source 17
    target 28
    preference 57
  ]
  edge [
    source 17
    target 29
    preference 15
  ]
  edge [
    source 17
    target 30
    preference 25
  ]
  edge [
    source 17
    target 31
    preference 62
  ]
  edge [
    source 17
    target 32
    preference 65
  ]
  edge [
    source 17
    target 33
    preference 1
  ]
  edge [
    source 18
    target 26
    preference 59
  ]
  edge [
    source 18
    target 27
    preference 54
  ]
  edge [
    source 18
    target 28
    preference 39
  ]
  edge [
    source 18
    target 29
    preference 55
  ]
  edge [
    source 18
    target 30
    preference 7
  ]
  edge [
    source 18
    target 31
    preference 27
  ]
  edge [
    source 18
    target 32
    preference 67
  ]
  edge [
    source 18
    target 33
    preference 1
  ]
  edge [
    source 19
    target 26
    preference 68
  ]
  edge [
    source 19
    target 27
    preference 21
  ]
  edge [
    source 19
    target 28
    preference 57
  ]
  edge [
    source 19
    target 29
    preference 47
  ]
  edge [
    source 19
    target 30
    preference 60
  ]
  edge [
    source 19
    target 31
    preference 42
  ]
  edge [
    source 19
    target 32
    preference 66
  ]
  edge [
    source 19
    target 33
    preference 1
  ]
  edge [
    source 20
    target 26
    preference 38
  ]
  edge [
    source 20
    target 27
    preference 64
  ]
  edge [
    source 20
    target 28
    preference 12
  ]
  edge [
    source 20
    target 29
    preference 44
  ]
  edge [
    source 20
    target 30
    preference 61
  ]
  edge [
    source 20
    target 31
    preference 67
  ]
  edge [
    source 20
    target 32
    preference 26
  ]
  edge [
    source 20
    target 33
    preference 1
  ]
  edge [
    source 21
    target 26
    preference 22
  ]
  edge [
    source 21
    target 27
    preference 45
  ]
  edge [
    source 21
    target 28
    preference 49
  ]
  edge [
    source 21
    target 29
    preference 22
  ]
  edge [
    source 21
    target 30
    preference 17
  ]
  edge [
    source 21
    target 31
    preference 7
  ]
  edge [
    source 21
    target 32
    preference 67
  ]
  edge [
    source 21
    target 33
    preference 1
  ]
  edge [
    source 22
    target 26
    preference 63
  ]
  edge [
    source 22
    target 27
    preference 3
  ]
  edge [
    source 22
    target 28
    preference 44
  ]
  edge [
    source 22
    target 29
    preference 19
  ]
  edge [
    source 22
    target 30
    preference 41
  ]
  edge [
    source 22
    target 31
    preference 42
  ]
  edge [
    source 22
    target 32
    preference 50
  ]
  edge [
    source 22
    target 33
    preference 1
  ]
  edge [
    source 23
    target 26
    preference 15
  ]
  edge [
    source 23
    target 27
    preference 20
  ]
  edge [
    source 23
    target 28
    preference 4
  ]
  edge [
    source 23
    target 29
    preference 62
  ]
  edge [
    source 23
    target 30
    preference 60
  ]
  edge [
    source 23
    target 31
    preference 57
  ]
  edge [
    source 23
    target 32
    preference 11
  ]
  edge [
    source 23
    target 33
    preference 1
  ]
  edge [
    source 24
    target 26
    preference 4
  ]
  edge [
    source 24
    target 27
    preference 35
  ]
  edge [
    source 24
    target 28
    preference 25
  ]
  edge [
    source 24
    target 29
    preference 40
  ]
  edge [
    source 24
    target 30
    preference 22
  ]
  edge [
    source 24
    target 31
    preference 19
  ]
  edge [
    source 24
    target 32
    preference 65
  ]
  edge [
    source 24
    target 33
    preference 1
  ]
  edge [
    source 25
    target 26
    preference 28
  ]
  edge [
    source 25
    target 27
    preference 36
  ]
  edge [
    source 25
    target 28
    preference 51
  ]
  edge [
    source 25
    target 29
    preference 4
  ]
  edge [
    source 25
    target 30
    preference 3
  ]
  edge [
    source 25
    target 31
    preference 52
  ]
  edge [
    source 25
    target 32
    preference 64
  ]
  edge [
    source 25
    target 33
    preference 1
  ]
  edge [
    source 26
    target 0
    preference 50
  ]
  edge [
    source 26
    target 1
    preference 49
  ]
  edge [
    source 26
    target 2
    preference 60
  ]
  edge [
    source 26
    target 3
    preference 26
  ]
  edge [
    source 26
    target 4
    preference 20
  ]
  edge [
    source 26
    target 5
    preference 29
  ]
  edge [
    source 26
    target 6
    preference 52
  ]
  edge [
    source 26
    target 7
    preference 52
  ]
  edge [
    source 26
    target 8
    preference 61
  ]
  edge [
    source 26
    target 9
    preference 62
  ]
  edge [
    source 26
    target 10
    preference 50
  ]
  edge [
    source 26
    target 11
    preference 37
  ]
  edge [
    source 26
    target 12
    preference 26
  ]
  edge [
    source 26
    target 13
    preference 3
  ]
  edge [
    source 26
    target 14
    preference 41
  ]
  edge [
    source 26
    target 15
    preference 17
  ]
  edge [
    source 26
    target 16
    preference 27
  ]
  edge [
    source 26
    target 17
    preference 7
  ]
  edge [
    source 26
    target 18
    preference 8
  ]
  edge [
    source 26
    target 19
    preference 53
  ]
  edge [
    source 26
    target 20
    preference 31
  ]
  edge [
    source 26
    target 21
    preference 60
  ]
  edge [
    source 26
    target 22
    preference 65
  ]
  edge [
    source 26
    target 23
    preference 15
  ]
  edge [
    source 26
    target 24
    preference 26
  ]
  edge [
    source 26
    target 25
    preference 13
  ]
  edge [
    source 27
    target 0
    preference 40
  ]
  edge [
    source 27
    target 1
    preference 19
  ]
  edge [
    source 27
    target 2
    preference 31
  ]
  edge [
    source 27
    target 3
    preference 48
  ]
  edge [
    source 27
    target 4
    preference 54
  ]
  edge [
    source 27
    target 5
    preference 48
  ]
  edge [
    source 27
    target 6
    preference 31
  ]
  edge [
    source 27
    target 7
    preference 28
  ]
  edge [
    source 27
    target 8
    preference 43
  ]
  edge [
    source 27
    target 9
    preference 30
  ]
  edge [
    source 27
    target 10
    preference 54
  ]
  edge [
    source 27
    target 11
    preference 13
  ]
  edge [
    source 27
    target 12
    preference 6
  ]
  edge [
    source 27
    target 13
    preference 12
  ]
  edge [
    source 27
    target 14
    preference 48
  ]
  edge [
    source 27
    target 15
    preference 59
  ]
  edge [
    source 27
    target 16
    preference 38
  ]
  edge [
    source 27
    target 17
    preference 46
  ]
  edge [
    source 27
    target 18
    preference 25
  ]
  edge [
    source 27
    target 19
    preference 8
  ]
  edge [
    source 27
    target 20
    preference 7
  ]
  edge [
    source 27
    target 21
    preference 10
  ]
  edge [
    source 27
    target 22
    preference 38
  ]
  edge [
    source 27
    target 23
    preference 48
  ]
  edge [
    source 27
    target 24
    preference 42
  ]
  edge [
    source 27
    target 25
    preference 24
  ]
  edge [
    source 28
    target 0
    preference 53
  ]
  edge [
    source 28
    target 1
    preference 25
  ]
  edge [
    source 28
    target 2
    preference 17
  ]
  edge [
    source 28
    target 3
    preference 38
  ]
  edge [
    source 28
    target 4
    preference 41
  ]
  edge [
    source 28
    target 5
    preference 9
  ]
  edge [
    source 28
    target 6
    preference 13
  ]
  edge [
    source 28
    target 7
    preference 37
  ]
  edge [
    source 28
    target 8
    preference 63
  ]
  edge [
    source 28
    target 9
    preference 64
  ]
  edge [
    source 28
    target 10
    preference 54
  ]
  edge [
    source 28
    target 11
    preference 60
  ]
  edge [
    source 28
    target 12
    preference 57
  ]
  edge [
    source 28
    target 13
    preference 59
  ]
  edge [
    source 28
    target 14
    preference 10
  ]
  edge [
    source 28
    target 15
    preference 60
  ]
  edge [
    source 28
    target 16
    preference 59
  ]
  edge [
    source 28
    target 17
    preference 30
  ]
  edge [
    source 28
    target 18
    preference 29
  ]
  edge [
    source 28
    target 19
    preference 64
  ]
  edge [
    source 28
    target 20
    preference 42
  ]
  edge [
    source 28
    target 21
    preference 46
  ]
  edge [
    source 28
    target 22
    preference 65
  ]
  edge [
    source 28
    target 23
    preference 42
  ]
  edge [
    source 28
    target 24
    preference 49
  ]
  edge [
    source 28
    target 25
    preference 46
  ]
  edge [
    source 29
    target 0
    preference 21
  ]
  edge [
    source 29
    target 1
    preference 50
  ]
  edge [
    source 29
    target 2
    preference 4
  ]
  edge [
    source 29
    target 3
    preference 25
  ]
  edge [
    source 29
    target 4
    preference 5
  ]
  edge [
    source 29
    target 5
    preference 14
  ]
  edge [
    source 29
    target 6
    preference 50
  ]
  edge [
    source 29
    target 7
    preference 39
  ]
  edge [
    source 29
    target 8
    preference 48
  ]
  edge [
    source 29
    target 9
    preference 6
  ]
  edge [
    source 29
    target 10
    preference 39
  ]
  edge [
    source 29
    target 11
    preference 61
  ]
  edge [
    source 29
    target 12
    preference 40
  ]
  edge [
    source 29
    target 13
    preference 52
  ]
  edge [
    source 29
    target 14
    preference 6
  ]
  edge [
    source 29
    target 15
    preference 8
  ]
  edge [
    source 29
    target 16
    preference 7
  ]
  edge [
    source 29
    target 17
    preference 21
  ]
  edge [
    source 29
    target 18
    preference 65
  ]
  edge [
    source 29
    target 19
    preference 48
  ]
  edge [
    source 29
    target 20
    preference 6
  ]
  edge [
    source 29
    target 21
    preference 15
  ]
  edge [
    source 29
    target 22
    preference 64
  ]
  edge [
    source 29
    target 23
    preference 54
  ]
  edge [
    source 29
    target 24
    preference 21
  ]
  edge [
    source 29
    target 25
    preference 45
  ]
  edge [
    source 30
    target 0
    preference 47
  ]
  edge [
    source 30
    target 1
    preference 43
  ]
  edge [
    source 30
    target 2
    preference 36
  ]
  edge [
    source 30
    target 3
    preference 41
  ]
  edge [
    source 30
    target 4
    preference 26
  ]
  edge [
    source 30
    target 5
    preference 57
  ]
  edge [
    source 30
    target 6
    preference 19
  ]
  edge [
    source 30
    target 7
    preference 11
  ]
  edge [
    source 30
    target 8
    preference 25
  ]
  edge [
    source 30
    target 9
    preference 44
  ]
  edge [
    source 30
    target 10
    preference 36
  ]
  edge [
    source 30
    target 11
    preference 56
  ]
  edge [
    source 30
    target 12
    preference 24
  ]
  edge [
    source 30
    target 13
    preference 46
  ]
  edge [
    source 30
    target 14
    preference 9
  ]
  edge [
    source 30
    target 15
    preference 12
  ]
  edge [
    source 30
    target 16
    preference 48
  ]
  edge [
    source 30
    target 17
    preference 39
  ]
  edge [
    source 30
    target 18
    preference 24
  ]
  edge [
    source 30
    target 19
    preference 48
  ]
  edge [
    source 30
    target 20
    preference 54
  ]
  edge [
    source 30
    target 21
    preference 22
  ]
  edge [
    source 30
    target 22
    preference 59
  ]
  edge [
    source 30
    target 23
    preference 40
  ]
  edge [
    source 30
    target 24
    preference 2
  ]
  edge [
    source 30
    target 25
    preference 43
  ]
  edge [
    source 31
    target 0
    preference 24
  ]
  edge [
    source 31
    target 1
    preference 29
  ]
  edge [
    source 31
    target 2
    preference 66
  ]
  edge [
    source 31
    target 3
    preference 45
  ]
  edge [
    source 31
    target 4
    preference 54
  ]
  edge [
    source 31
    target 5
    preference 63
  ]
  edge [
    source 31
    target 6
    preference 68
  ]
  edge [
    source 31
    target 7
    preference 2
  ]
  edge [
    source 31
    target 8
    preference 32
  ]
  edge [
    source 31
    target 9
    preference 38
  ]
  edge [
    source 31
    target 10
    preference 44
  ]
  edge [
    source 31
    target 11
    preference 11
  ]
  edge [
    source 31
    target 12
    preference 62
  ]
  edge [
    source 31
    target 13
    preference 52
  ]
  edge [
    source 31
    target 14
    preference 63
  ]
  edge [
    source 31
    target 15
    preference 18
  ]
  edge [
    source 31
    target 16
    preference 31
  ]
  edge [
    source 31
    target 17
    preference 22
  ]
  edge [
    source 31
    target 18
    preference 10
  ]
  edge [
    source 31
    target 19
    preference 67
  ]
  edge [
    source 31
    target 20
    preference 39
  ]
  edge [
    source 31
    target 21
    preference 60
  ]
  edge [
    source 31
    target 22
    preference 66
  ]
  edge [
    source 31
    target 23
    preference 65
  ]
  edge [
    source 31
    target 24
    preference 22
  ]
  edge [
    source 31
    target 25
    preference 12
  ]
  edge [
    source 32
    target 0
    preference 4
  ]
  edge [
    source 32
    target 1
    preference 23
  ]
  edge [
    source 32
    target 2
    preference 27
  ]
  edge [
    source 32
    target 3
    preference 33
  ]
  edge [
    source 32
    target 4
    preference 61
  ]
  edge [
    source 32
    target 5
    preference 10
  ]
  edge [
    source 32
    target 6
    preference 15
  ]
  edge [
    source 32
    target 7
    preference 62
  ]
  edge [
    source 32
    target 8
    preference 4
  ]
  edge [
    source 32
    target 9
    preference 10
  ]
  edge [
    source 32
    target 10
    preference 29
  ]
  edge [
    source 32
    target 11
    preference 11
  ]
  edge [
    source 32
    target 12
    preference 13
  ]
  edge [
    source 32
    target 13
    preference 46
  ]
  edge [
    source 32
    target 14
    preference 61
  ]
  edge [
    source 32
    target 15
    preference 46
  ]
  edge [
    source 32
    target 16
    preference 11
  ]
  edge [
    source 32
    target 17
    preference 24
  ]
  edge [
    source 32
    target 18
    preference 52
  ]
  edge [
    source 32
    target 19
    preference 60
  ]
  edge [
    source 32
    target 20
    preference 40
  ]
  edge [
    source 32
    target 21
    preference 59
  ]
  edge [
    source 32
    target 22
    preference 7
  ]
  edge [
    source 32
    target 23
    preference 6
  ]
  edge [
    source 32
    target 24
    preference 55
  ]
  edge [
    source 32
    target 25
    preference 49
  ]
  edge [
    source 33
    target 0
    preference 1
  ]
  edge [
    source 33
    target 1
    preference 1
  ]
  edge [
    source 33
    target 2
    preference 1
  ]
  edge [
    source 33
    target 3
    preference 1
  ]
  edge [
    source 33
    target 4
    preference 1
  ]
  edge [
    source 33
    target 5
    preference 1
  ]
  edge [
    source 33
    target 6
    preference 1
  ]
  edge [
    source 33
    target 7
    preference 1
  ]
  edge [
    source 33
    target 8
    preference 1
  ]
  edge [
    source 33
    target 9
    preference 1
  ]
  edge [
    source 33
    target 10
    preference 1
  ]
  edge [
    source 33
    target 11
    preference 1
  ]
  edge [
    source 33
    target 12
    preference 1
  ]
  edge [
    source 33
    target 13
    preference 1
  ]
  edge [
    source 33
    target 14
    preference 1
  ]
  edge [
    source 33
    target 15
    preference 1
  ]
  edge [
    source 33
    target 16
    preference 1
  ]
  edge [
    source 33
    target 17
    preference 1
  ]
  edge [
    source 33
    target 18
    preference 1
  ]
  edge [
    source 33
    target 19
    preference 1
  ]
  edge [
    source 33
    target 20
    preference 1
  ]
  edge [
    source 33
    target 21
    preference 1
  ]
  edge [
    source 33
    target 22
    preference 1
  ]
  edge [
    source 33
    target 23
    preference 1
  ]
  edge [
    source 33
    target 24
    preference 1
  ]
  edge [
    source 33
    target 25
    preference 1
  ]
]
