graph [
  directed 1
  name "complete_bipartite_graph(15,3)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 16
    label "16"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 17
    label "17"
    bipartite 1
    availableSeats 7
    waitingList 1
  ]
  edge [
    source 0
    target 15
    preference 8
  ]
  edge [
    source 0
    target 16
    preference 6
  ]
  edge [
    source 0
    target 17
    preference 1
  ]
  edge [
    source 1
    target 15
    preference 31
  ]
  edge [
    source 1
    target 16
    preference 9
  ]
  edge [
    source 1
    target 17
    preference 1
  ]
  edge [
    source 2
    target 15
    preference 24
  ]
  edge [
    source 2
    target 16
    preference 11
  ]
  edge [
    source 2
    target 17
    preference 1
  ]
  edge [
    source 3
    target 15
    preference 17
  ]
  edge [
    source 3
    target 16
    preference 29
  ]
  edge [
    source 3
    target 17
    preference 1
  ]
  edge [
    source 4
    target 15
    preference 32
  ]
  edge [
    source 4
    target 16
    preference 5
  ]
  edge [
    source 4
    target 17
    preference 1
  ]
  edge [
    source 5
    target 15
    preference 8
  ]
  edge [
    source 5
    target 16
    preference 16
  ]
  edge [
    source 5
    target 17
    preference 1
  ]
  edge [
    source 6
    target 15
    preference 15
  ]
  edge [
    source 6
    target 16
    preference 23
  ]
  edge [
    source 6
    target 17
    preference 1
  ]
  edge [
    source 7
    target 15
    preference 11
  ]
  edge [
    source 7
    target 16
    preference 28
  ]
  edge [
    source 7
    target 17
    preference 1
  ]
  edge [
    source 8
    target 15
    preference 29
  ]
  edge [
    source 8
    target 16
    preference 15
  ]
  edge [
    source 8
    target 17
    preference 1
  ]
  edge [
    source 9
    target 15
    preference 2
  ]
  edge [
    source 9
    target 16
    preference 28
  ]
  edge [
    source 9
    target 17
    preference 1
  ]
  edge [
    source 10
    target 15
    preference 11
  ]
  edge [
    source 10
    target 16
    preference 23
  ]
  edge [
    source 10
    target 17
    preference 1
  ]
  edge [
    source 11
    target 15
    preference 19
  ]
  edge [
    source 11
    target 16
    preference 9
  ]
  edge [
    source 11
    target 17
    preference 1
  ]
  edge [
    source 12
    target 15
    preference 3
  ]
  edge [
    source 12
    target 16
    preference 28
  ]
  edge [
    source 12
    target 17
    preference 1
  ]
  edge [
    source 13
    target 15
    preference 29
  ]
  edge [
    source 13
    target 16
    preference 30
  ]
  edge [
    source 13
    target 17
    preference 1
  ]
  edge [
    source 14
    target 15
    preference 4
  ]
  edge [
    source 14
    target 16
    preference 5
  ]
  edge [
    source 14
    target 17
    preference 1
  ]
  edge [
    source 15
    target 0
    preference 24
  ]
  edge [
    source 15
    target 1
    preference 35
  ]
  edge [
    source 15
    target 2
    preference 11
  ]
  edge [
    source 15
    target 3
    preference 29
  ]
  edge [
    source 15
    target 4
    preference 17
  ]
  edge [
    source 15
    target 5
    preference 30
  ]
  edge [
    source 15
    target 6
    preference 10
  ]
  edge [
    source 15
    target 7
    preference 8
  ]
  edge [
    source 15
    target 8
    preference 19
  ]
  edge [
    source 15
    target 9
    preference 15
  ]
  edge [
    source 15
    target 10
    preference 15
  ]
  edge [
    source 15
    target 11
    preference 13
  ]
  edge [
    source 15
    target 12
    preference 31
  ]
  edge [
    source 15
    target 13
    preference 3
  ]
  edge [
    source 15
    target 14
    preference 34
  ]
  edge [
    source 16
    target 0
    preference 8
  ]
  edge [
    source 16
    target 1
    preference 13
  ]
  edge [
    source 16
    target 2
    preference 2
  ]
  edge [
    source 16
    target 3
    preference 6
  ]
  edge [
    source 16
    target 4
    preference 13
  ]
  edge [
    source 16
    target 5
    preference 35
  ]
  edge [
    source 16
    target 6
    preference 16
  ]
  edge [
    source 16
    target 7
    preference 24
  ]
  edge [
    source 16
    target 8
    preference 11
  ]
  edge [
    source 16
    target 9
    preference 8
  ]
  edge [
    source 16
    target 10
    preference 9
  ]
  edge [
    source 16
    target 11
    preference 5
  ]
  edge [
    source 16
    target 12
    preference 17
  ]
  edge [
    source 16
    target 13
    preference 2
  ]
  edge [
    source 16
    target 14
    preference 32
  ]
  edge [
    source 17
    target 0
    preference 1
  ]
  edge [
    source 17
    target 1
    preference 1
  ]
  edge [
    source 17
    target 2
    preference 1
  ]
  edge [
    source 17
    target 3
    preference 1
  ]
  edge [
    source 17
    target 4
    preference 1
  ]
  edge [
    source 17
    target 5
    preference 1
  ]
  edge [
    source 17
    target 6
    preference 1
  ]
  edge [
    source 17
    target 7
    preference 1
  ]
  edge [
    source 17
    target 8
    preference 1
  ]
  edge [
    source 17
    target 9
    preference 1
  ]
  edge [
    source 17
    target 10
    preference 1
  ]
  edge [
    source 17
    target 11
    preference 1
  ]
  edge [
    source 17
    target 12
    preference 1
  ]
  edge [
    source 17
    target 13
    preference 1
  ]
  edge [
    source 17
    target 14
    preference 1
  ]
]
