graph [
  directed 1
  name "complete_bipartite_graph(20,5)"
  node [
    id 0
    label "0"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 1
    label "1"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 2
    label "2"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 3
    label "3"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 4
    label "4"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 5
    label "5"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 6
    label "6"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 7
    label "7"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 8
    label "8"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 9
    label "9"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 10
    label "10"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 11
    label "11"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 12
    label "12"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 13
    label "13"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 14
    label "14"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 15
    label "15"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 16
    label "16"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 17
    label "17"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 18
    label "18"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 19
    label "19"
    bipartite 0
    requestedSeats 1
  ]
  node [
    id 20
    label "20"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 21
    label "21"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 22
    label "22"
    bipartite 1
    availableSeats 2
  ]
  node [
    id 23
    label "23"
    bipartite 1
    availableSeats 4
  ]
  node [
    id 24
    label "24"
    bipartite 1
    availableSeats 6
    waitingList 1
  ]
  edge [
    source 0
    target 20
    preference 14
  ]
  edge [
    source 0
    target 21
    preference 34
  ]
  edge [
    source 0
    target 22
    preference 22
  ]
  edge [
    source 0
    target 23
    preference 20
  ]
  edge [
    source 0
    target 24
    preference 1
  ]
  edge [
    source 1
    target 20
    preference 23
  ]
  edge [
    source 1
    target 21
    preference 42
  ]
  edge [
    source 1
    target 22
    preference 10
  ]
  edge [
    source 1
    target 23
    preference 41
  ]
  edge [
    source 1
    target 24
    preference 1
  ]
  edge [
    source 2
    target 20
    preference 14
  ]
  edge [
    source 2
    target 21
    preference 2
  ]
  edge [
    source 2
    target 22
    preference 14
  ]
  edge [
    source 2
    target 23
    preference 35
  ]
  edge [
    source 2
    target 24
    preference 1
  ]
  edge [
    source 3
    target 20
    preference 29
  ]
  edge [
    source 3
    target 21
    preference 35
  ]
  edge [
    source 3
    target 22
    preference 13
  ]
  edge [
    source 3
    target 23
    preference 20
  ]
  edge [
    source 3
    target 24
    preference 1
  ]
  edge [
    source 4
    target 20
    preference 21
  ]
  edge [
    source 4
    target 21
    preference 47
  ]
  edge [
    source 4
    target 22
    preference 23
  ]
  edge [
    source 4
    target 23
    preference 4
  ]
  edge [
    source 4
    target 24
    preference 1
  ]
  edge [
    source 5
    target 20
    preference 28
  ]
  edge [
    source 5
    target 21
    preference 7
  ]
  edge [
    source 5
    target 22
    preference 50
  ]
  edge [
    source 5
    target 23
    preference 6
  ]
  edge [
    source 5
    target 24
    preference 1
  ]
  edge [
    source 6
    target 20
    preference 28
  ]
  edge [
    source 6
    target 21
    preference 33
  ]
  edge [
    source 6
    target 22
    preference 12
  ]
  edge [
    source 6
    target 23
    preference 41
  ]
  edge [
    source 6
    target 24
    preference 1
  ]
  edge [
    source 7
    target 20
    preference 32
  ]
  edge [
    source 7
    target 21
    preference 50
  ]
  edge [
    source 7
    target 22
    preference 49
  ]
  edge [
    source 7
    target 23
    preference 13
  ]
  edge [
    source 7
    target 24
    preference 1
  ]
  edge [
    source 8
    target 20
    preference 20
  ]
  edge [
    source 8
    target 21
    preference 19
  ]
  edge [
    source 8
    target 22
    preference 44
  ]
  edge [
    source 8
    target 23
    preference 26
  ]
  edge [
    source 8
    target 24
    preference 1
  ]
  edge [
    source 9
    target 20
    preference 46
  ]
  edge [
    source 9
    target 21
    preference 33
  ]
  edge [
    source 9
    target 22
    preference 26
  ]
  edge [
    source 9
    target 23
    preference 3
  ]
  edge [
    source 9
    target 24
    preference 1
  ]
  edge [
    source 10
    target 20
    preference 8
  ]
  edge [
    source 10
    target 21
    preference 16
  ]
  edge [
    source 10
    target 22
    preference 11
  ]
  edge [
    source 10
    target 23
    preference 47
  ]
  edge [
    source 10
    target 24
    preference 1
  ]
  edge [
    source 11
    target 20
    preference 6
  ]
  edge [
    source 11
    target 21
    preference 33
  ]
  edge [
    source 11
    target 22
    preference 34
  ]
  edge [
    source 11
    target 23
    preference 3
  ]
  edge [
    source 11
    target 24
    preference 1
  ]
  edge [
    source 12
    target 20
    preference 9
  ]
  edge [
    source 12
    target 21
    preference 3
  ]
  edge [
    source 12
    target 22
    preference 41
  ]
  edge [
    source 12
    target 23
    preference 49
  ]
  edge [
    source 12
    target 24
    preference 1
  ]
  edge [
    source 13
    target 20
    preference 2
  ]
  edge [
    source 13
    target 21
    preference 47
  ]
  edge [
    source 13
    target 22
    preference 35
  ]
  edge [
    source 13
    target 23
    preference 27
  ]
  edge [
    source 13
    target 24
    preference 1
  ]
  edge [
    source 14
    target 20
    preference 2
  ]
  edge [
    source 14
    target 21
    preference 44
  ]
  edge [
    source 14
    target 22
    preference 19
  ]
  edge [
    source 14
    target 23
    preference 4
  ]
  edge [
    source 14
    target 24
    preference 1
  ]
  edge [
    source 15
    target 20
    preference 11
  ]
  edge [
    source 15
    target 21
    preference 16
  ]
  edge [
    source 15
    target 22
    preference 37
  ]
  edge [
    source 15
    target 23
    preference 2
  ]
  edge [
    source 15
    target 24
    preference 1
  ]
  edge [
    source 16
    target 20
    preference 19
  ]
  edge [
    source 16
    target 21
    preference 33
  ]
  edge [
    source 16
    target 22
    preference 21
  ]
  edge [
    source 16
    target 23
    preference 8
  ]
  edge [
    source 16
    target 24
    preference 1
  ]
  edge [
    source 17
    target 20
    preference 9
  ]
  edge [
    source 17
    target 21
    preference 20
  ]
  edge [
    source 17
    target 22
    preference 46
  ]
  edge [
    source 17
    target 23
    preference 22
  ]
  edge [
    source 17
    target 24
    preference 1
  ]
  edge [
    source 18
    target 20
    preference 47
  ]
  edge [
    source 18
    target 21
    preference 43
  ]
  edge [
    source 18
    target 22
    preference 42
  ]
  edge [
    source 18
    target 23
    preference 6
  ]
  edge [
    source 18
    target 24
    preference 1
  ]
  edge [
    source 19
    target 20
    preference 33
  ]
  edge [
    source 19
    target 21
    preference 28
  ]
  edge [
    source 19
    target 22
    preference 49
  ]
  edge [
    source 19
    target 23
    preference 12
  ]
  edge [
    source 19
    target 24
    preference 1
  ]
  edge [
    source 20
    target 0
    preference 29
  ]
  edge [
    source 20
    target 1
    preference 49
  ]
  edge [
    source 20
    target 2
    preference 12
  ]
  edge [
    source 20
    target 3
    preference 47
  ]
  edge [
    source 20
    target 4
    preference 48
  ]
  edge [
    source 20
    target 5
    preference 16
  ]
  edge [
    source 20
    target 6
    preference 13
  ]
  edge [
    source 20
    target 7
    preference 46
  ]
  edge [
    source 20
    target 8
    preference 17
  ]
  edge [
    source 20
    target 9
    preference 35
  ]
  edge [
    source 20
    target 10
    preference 13
  ]
  edge [
    source 20
    target 11
    preference 41
  ]
  edge [
    source 20
    target 12
    preference 10
  ]
  edge [
    source 20
    target 13
    preference 2
  ]
  edge [
    source 20
    target 14
    preference 43
  ]
  edge [
    source 20
    target 15
    preference 35
  ]
  edge [
    source 20
    target 16
    preference 2
  ]
  edge [
    source 20
    target 17
    preference 29
  ]
  edge [
    source 20
    target 18
    preference 17
  ]
  edge [
    source 20
    target 19
    preference 19
  ]
  edge [
    source 21
    target 0
    preference 13
  ]
  edge [
    source 21
    target 1
    preference 25
  ]
  edge [
    source 21
    target 2
    preference 11
  ]
  edge [
    source 21
    target 3
    preference 36
  ]
  edge [
    source 21
    target 4
    preference 24
  ]
  edge [
    source 21
    target 5
    preference 26
  ]
  edge [
    source 21
    target 6
    preference 42
  ]
  edge [
    source 21
    target 7
    preference 32
  ]
  edge [
    source 21
    target 8
    preference 24
  ]
  edge [
    source 21
    target 9
    preference 31
  ]
  edge [
    source 21
    target 10
    preference 36
  ]
  edge [
    source 21
    target 11
    preference 31
  ]
  edge [
    source 21
    target 12
    preference 18
  ]
  edge [
    source 21
    target 13
    preference 10
  ]
  edge [
    source 21
    target 14
    preference 40
  ]
  edge [
    source 21
    target 15
    preference 44
  ]
  edge [
    source 21
    target 16
    preference 11
  ]
  edge [
    source 21
    target 17
    preference 13
  ]
  edge [
    source 21
    target 18
    preference 33
  ]
  edge [
    source 21
    target 19
    preference 21
  ]
  edge [
    source 22
    target 0
    preference 46
  ]
  edge [
    source 22
    target 1
    preference 47
  ]
  edge [
    source 22
    target 2
    preference 34
  ]
  edge [
    source 22
    target 3
    preference 4
  ]
  edge [
    source 22
    target 4
    preference 40
  ]
  edge [
    source 22
    target 5
    preference 17
  ]
  edge [
    source 22
    target 6
    preference 7
  ]
  edge [
    source 22
    target 7
    preference 35
  ]
  edge [
    source 22
    target 8
    preference 23
  ]
  edge [
    source 22
    target 9
    preference 9
  ]
  edge [
    source 22
    target 10
    preference 18
  ]
  edge [
    source 22
    target 11
    preference 47
  ]
  edge [
    source 22
    target 12
    preference 12
  ]
  edge [
    source 22
    target 13
    preference 30
  ]
  edge [
    source 22
    target 14
    preference 29
  ]
  edge [
    source 22
    target 15
    preference 49
  ]
  edge [
    source 22
    target 16
    preference 13
  ]
  edge [
    source 22
    target 17
    preference 30
  ]
  edge [
    source 22
    target 18
    preference 2
  ]
  edge [
    source 22
    target 19
    preference 24
  ]
  edge [
    source 23
    target 0
    preference 2
  ]
  edge [
    source 23
    target 1
    preference 21
  ]
  edge [
    source 23
    target 2
    preference 13
  ]
  edge [
    source 23
    target 3
    preference 34
  ]
  edge [
    source 23
    target 4
    preference 32
  ]
  edge [
    source 23
    target 5
    preference 7
  ]
  edge [
    source 23
    target 6
    preference 28
  ]
  edge [
    source 23
    target 7
    preference 39
  ]
  edge [
    source 23
    target 8
    preference 31
  ]
  edge [
    source 23
    target 9
    preference 35
  ]
  edge [
    source 23
    target 10
    preference 45
  ]
  edge [
    source 23
    target 11
    preference 41
  ]
  edge [
    source 23
    target 12
    preference 48
  ]
  edge [
    source 23
    target 13
    preference 3
  ]
  edge [
    source 23
    target 14
    preference 6
  ]
  edge [
    source 23
    target 15
    preference 28
  ]
  edge [
    source 23
    target 16
    preference 13
  ]
  edge [
    source 23
    target 17
    preference 47
  ]
  edge [
    source 23
    target 18
    preference 42
  ]
  edge [
    source 23
    target 19
    preference 41
  ]
  edge [
    source 24
    target 0
    preference 1
  ]
  edge [
    source 24
    target 1
    preference 1
  ]
  edge [
    source 24
    target 2
    preference 1
  ]
  edge [
    source 24
    target 3
    preference 1
  ]
  edge [
    source 24
    target 4
    preference 1
  ]
  edge [
    source 24
    target 5
    preference 1
  ]
  edge [
    source 24
    target 6
    preference 1
  ]
  edge [
    source 24
    target 7
    preference 1
  ]
  edge [
    source 24
    target 8
    preference 1
  ]
  edge [
    source 24
    target 9
    preference 1
  ]
  edge [
    source 24
    target 10
    preference 1
  ]
  edge [
    source 24
    target 11
    preference 1
  ]
  edge [
    source 24
    target 12
    preference 1
  ]
  edge [
    source 24
    target 13
    preference 1
  ]
  edge [
    source 24
    target 14
    preference 1
  ]
  edge [
    source 24
    target 15
    preference 1
  ]
  edge [
    source 24
    target 16
    preference 1
  ]
  edge [
    source 24
    target 17
    preference 1
  ]
  edge [
    source 24
    target 18
    preference 1
  ]
  edge [
    source 24
    target 19
    preference 1
  ]
]
